import { useEffect, useState } from 'react';
import { getReaders } from '@api';

// readers
const useReaders = () => {
  const [res, setRes] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchReaders = async () => {
      setLoading(true);
      // const order = [];
      const data = {
        currentPage: 0, // page,
        pageSize: 10, //pageSize,
        filterQuery: "",
        order: [
          {
            column: "id",
            dir: 'asc',
          },
        ],
      };
      
      const response = await getReaders({data});

      if (response.ok) {
        setRes(response.data.content);
      }
      setLoading(false);
    };

    fetchReaders();
  }, []);

  return [
    {
      res,
      loading,
    },
  ];
};

export default useReaders;
