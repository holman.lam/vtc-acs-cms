import { useEffect, useState } from 'react';
import { getLocations } from '@api';

const useLocations = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchLocations = async () => {
      setLoading(true);
      const response = await getLocations();

      if (response.ok) {
        setData(response.data);
      }
      setLoading(false);
    };

    fetchLocations();
  }, []);

  return [
    {
      data,
      loading,
    },
  ];
};

export default useLocations;
