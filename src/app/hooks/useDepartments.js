import { useEffect, useState } from 'react';
import { getDepartments } from '@api';

// departments
const useDepartments = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchDepartments = async () => {
      setLoading(true);
      const response = await getDepartments();

      if (response.ok) {
        setData(response.data);
      }
      setLoading(false);
    };

    fetchDepartments();
  }, []);

  return [
    {
      data,
      loading,
    },
  ];
};

export default useDepartments;
