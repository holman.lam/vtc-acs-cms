import { format, formatISO, getHours, getMinutes, parseISO, set } from 'date-fns';
import moment from 'moment';

export const convertApiDateTimeToDisplayDateTime = (datetime) => {
  if (!datetime) {
    return '';
  }
  return format(parseISO(datetime), 'yyyy.MM.dd, HH:mm');
};

export const convertApiDateTimeToDisplayDate = (datetime) => {
  if (!datetime) {
    return '';
  }
  return format(parseISO(datetime), 'yyyy.MM.dd');
};

export const convertApiDateTimeToDate = (datetime) => {
  if (!datetime) {
    return '';
  }
  return parseISO(datetime);
};

export const combineDateAndTimeToDateTime = (date, time) => {
  return format(
    set(date, {
      hours: getHours(time),
      minutes: getMinutes(time),
      seconds: 0,
      milliseconds: 0,
    }),
    "yyyy-MM-dd'T'HH:mm:ss"
  );
};

export const extractDateFromDateTime = (datetime) => {
  return moment(datetime).format('yyyy-MM-DD');
};
export const extractTimeFromDateTime = (datetime) => {
  return moment(datetime).format('HH:mm:ss');
};

export const splitTime = (datetime) => {
  const day = moment(datetime);
  return {
    hour: day.format("HH"),
    minute: day.format("mm"),
    second: day.format("ss"),
    nano: "00",
  };
};