import IconButton from '@mui/material/IconButton';
import FuseSvgIcon from '@fuse/core/FuseSvgIcon';
import { NavLink } from 'react-router-dom';

function LogoutButton(props) {
  return (
    <IconButton className="w-40 h-40 mx-24" size="large" component={NavLink} to="/logout">
      {props.children}
    </IconButton>
  );
}

LogoutButton.defaultProps = {
  children: <FuseSvgIcon>heroicons-outline:logout</FuseSvgIcon>,
};

export default LogoutButton;
