import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { useSelector } from 'react-redux';
import { selectUser } from 'app/store/userSlice';

function UserMenu(props) {
  const user = useSelector(selectUser);

  return (
    <>
      <Avatar className="md:mx-4">{user.username[0]}</Avatar>
      <div className="hidden md:flex flex-col mx-4">
        <Typography className="text-11 font-medium capitalize" color="secondary">
          {user.role.toString()}
          {(!user.role || (Array.isArray(user.role) && user.role.length === 0)) && 'Guest'}
        </Typography>
        <Typography component="span" className="font-semibold flex">
          {user.username}
        </Typography>
      </div>
    </>
  );
}

export default UserMenu;
