import FuseScrollbars from '@fuse/core/FuseScrollbars';
import MuiTable from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import { forwardRef, useEffect, useState, useImperativeHandle, useCallback } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import IconButton from '@mui/material/IconButton';
import Checkbox from '@mui/material/Checkbox';
import _ from 'lodash';

const Table = forwardRef(
  (
    {
      columns,
      dataSource,
      defaultSort = { direction: 'asc', id: null },
      defaultPageSize = 10,
      actions = undefined,
      selection = false,
      selected = [],
      isSelected = () => {},
      onSelect = () => {},
      onSelectAll = () => {},
      actionBar = undefined,
    },
    ref
  ) => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [page, setPage] = useState(0);
    const [pageSize, setPageSize] = useState(defaultPageSize);
    const [data, setData] = useState([]);
    const [count, setCount] = useState(0);
    const [sort, setSort] = useState(defaultSort);

    function loadData() {
      setLoading(true);
      dataSource({
        page,
        pageSize,
        sort,
      })
        .then((response) => {
          setData(response.data);
          setCount(response.count);
          setLoading(false);
        })
        .catch((e) => {
          setData([]);
          setCount(0);
          setError(e);
          setLoading(false);
        });
    }

    const handleLoadData = useCallback(loadData, [dataSource, page, pageSize, sort]);

    useImperativeHandle(ref, () => ({
      reload() {
        handleLoadData();
      },
    }));

    useEffect(() => {
      handleLoadData();
    }, [page, pageSize, sort]);

    const createSortHandler = (property) => (event) => {
      const id = property;
      let direction = 'desc';

      if (sort.id === property && sort.direction === 'desc') {
        direction = 'asc';
      }

      setSort({
        direction,
        id,
      });
    };

    return (
      <div className="w-full flex flex-col min-h-full">
        <FuseScrollbars className="grow overflow-x-auto">
          <MuiTable stickyHeader className="min-w-lg" aria-labelledby="tableTitle">
            <TableHead>
              <TableRow>
                {selection && (
                  <TableCell padding="checkbox">
                    <Checkbox
                      color="primary"
                      indeterminate={selected.length > 0 && selected.length < count}
                      checked={count > 0 && selected.length === count}
                      onChange={(event) => onSelectAll(event, data)}
                      inputProps={{
                        'aria-label': 'select all desserts',
                      }}
                    />
                  </TableCell>
                )}
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    sortDirection={sort.id === column.id ? sort.direction : false}
                  >
                    {column.sorting === false ? (
                      column.label
                    ) : (
                      <TableSortLabel
                        active={sort.id === column.id}
                        direction={sort.direction}
                        onClick={createSortHandler(column.id)}
                      >
                        {column.label}
                      </TableSortLabel>
                    )}
                  </TableCell>
                ))}
                {actions && actions.length > 0 && <TableCell />}
              </TableRow>

              {actionBar && (
                <TableRow>
                  <TableCell
                    colSpan={
                      (selection ? 1 : 0) + columns.length + (actions && actions.length > 0 ? 1 : 0)
                    }
                    sx={{ py: 0 }}
                  >
                    {actionBar}
                  </TableCell>
                </TableRow>
              )}
            </TableHead>

            <TableBody>
              {loading ? (
                <TableRow>
                  <TableCell colSpan={columns.length}>
                    <FuseLoading />
                  </TableCell>
                </TableRow>
              ) : (
                data.map((row, index) => {
                  const isItemSelected = isSelected?.(row);
                  return (
                    <TableRow tabIndex={-1} key={`row-${page * pageSize + index}`}>
                      {selection && (
                        <TableCell padding="checkbox">
                          <Checkbox
                            color="primary"
                            checked={isItemSelected}
                            onClick={() => onSelect(row)}
                          />
                        </TableCell>
                      )}

                      {columns.map((column) => {
                        if (column.render) {
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.render({ rowData: row })}
                            </TableCell>
                          );
                        }
                        const value = _.get(row, column.id);
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === 'number'
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}

                      {actions && actions.length > 0 && (
                        <TableCell>
                          {actions.map((a, i) => {
                            const action = a(row);
                            return (
                              <IconButton
                                key={`row-${page * pageSize + index}-action-${i}`}
                                size="small"
                                onClick={action.onClick}
                              >
                                {action.icon}
                              </IconButton>
                            );
                          })}
                        </TableCell>
                      )}
                    </TableRow>
                  );
                })
              )}
            </TableBody>
          </MuiTable>
        </FuseScrollbars>

        <TablePagination
          className="shrink-0 border-t-1"
          component="div"
          count={count}
          rowsPerPage={pageSize}
          page={page}
          onPageChange={(e, p) => setPage(p)}
          onRowsPerPageChange={(e) => setPageSize(e.target.value)}
        />
      </div>
    );
  }
);

export default Table;
