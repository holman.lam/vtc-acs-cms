export const lightPaletteText = {
  primary: 'rgb(17, 24, 39)',
  secondary: 'rgb(107, 114, 128)',
  disabled: 'rgb(149, 156, 169)',
};

export const darkPaletteText = {
  primary: 'rgb(255,255,255)',
  secondary: 'rgb(148, 163, 184)',
  disabled: 'rgb(156, 163, 175)',
};

const themesConfig = {
  customise: {
    components: {
      MuiButton: {
        defaultProps: {
          variant: 'contained',
          color: 'primary',
        },
      },
      MuiDialogActions: {
        styleOverrides: {
          root: {
            justifyContent: 'flex-start',
          },
        },
      },
      MuiTableCell: {
        styleOverrides: {
          root: ({ theme, ownerState }) => ({
            borderBottom: 'none',
            ...(ownerState.variant === 'head' && {
              color: theme.palette.primary.main,
            }),
          }),
        },
      },
      MuiTableSortLabel: {
        styleOverrides: {
          root: ({ theme, ownerState }) => ({
            '&:hover': {
              color: `${theme.palette.primary.main} !important`,
            },
          }),
          active: ({ theme, ownerState }) => ({
            color: `${theme.palette.primary.main} !important`,
          }),
          icon: ({ theme, ownerState }) => ({
            color: `${theme.palette.primary.main} !important`,
          }),
        },
      },
    },
    palette: {
      mode: 'light',
      divider: '#e2e8f0',
      text: lightPaletteText,
      common: {
        black: 'rgb(17, 24, 39)',
        white: 'rgb(255, 255, 255)',
      },
      primary: {
        light: '#82CBF1',
        main: '#03aed9',
        dark: '#007fa7',
        contrastText: darkPaletteText.primary,
      },
      secondary: {
        light: '#82CBF1',
        main: '#03aed9',
        dark: '#007fa7',
        contrastText: darkPaletteText.primary,
      },
      background: {
        paper: '#FFFFFF',
        default: '#F6F7F9',
      },
      error: {
        light: '#ffcdd2',
        main: '#f44336',
        dark: '#b71c1c',
      },
    },
    status: {
      danger: 'orange',
    },
  },
  navbar: {
    palette: {
      mode: 'dark',
      divider: '#e2e8f0',
      text: darkPaletteText,
      common: {
        black: 'rgb(17, 24, 39)',
        white: 'rgb(255, 255, 255)',
      },
      primary: {
        light: '#82CBF1',
        main: '#03aed9',
        dark: '#007fa7',
        contrastText: darkPaletteText.primary,
      },
      secondary: {
        light: '#82CBF1',
        main: '#03aed9',
        dark: '#007fa7',
        contrastText: darkPaletteText.primary,
      },
      background: {
        paper: '#FFFFFF',
        default: '#03aed9',
      },
      error: {
        light: '#ffcdd2',
        main: '#f44336',
        dark: '#b71c1c',
      },
    },
    status: {
      danger: 'orange',
    },
  },
};

export default themesConfig;
