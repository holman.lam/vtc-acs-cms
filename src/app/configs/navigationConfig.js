const navigationConfig = [
  // {
  //   id: 'dashboards',
  //   title: 'Dashboards',
  //   type: 'item',
  //   icon: 'dashboards',
  //   url: 'dashboards',
  // },
  {
    id: 'identity',
    title: 'Identity',
    type: 'collapse',
    icon: 'identity',
    children: [
      {
        id: 'identity.list',
        title: 'List',
        type: 'item',
        url: '/identity/list',
      },
      {
        id: 'identity.approval',
        title: 'Approval',
        type: 'item',
        url: '/identity/approval',
        badge: {
          title: '2',
          classes: 'bg-orange-600 text-white rounded-full',
        },
      },
    ],
  },
  {
    id: 'access-report',
    title: 'Access Report',
    type: 'item',
    icon: 'access-report',
    url: 'access-report',
  },
  {
    id: 'reader',
    title: 'Reader',
    type: 'item',
    icon: 'reader',
    url: 'reader',
  },
  {
    id: 'gate-scheduler',
    title: 'Gate Scheduler',
    type: 'item',
    icon: 'gate-scheduler',
    url: 'gate-scheduler',
  },
  {
    id: 'data-synchronization',
    title: 'Data Synchronization',
    type: 'item',
    icon: 'data-synchronization',
    url: 'data-synchronization',
  },
  {
    id: 'custom-reserved-fields',
    title: 'Custom Reserved Fields',
    type: 'item',
    icon: 'custom-reserved-fields',
    url: 'custom-reserved-fields',
  },
  {
    id: 'account',
    title: 'Account',
    type: 'collapse',
    icon: 'account',
    children: [
      {
        id: 'account.user',
        title: 'User',
        type: 'item',
        url: 'account/user',
      },
      {
        id: 'account.app',
        title: 'App',
        type: 'item',
        url: 'account/app',
      },
    ],
  },
];

export default navigationConfig;
