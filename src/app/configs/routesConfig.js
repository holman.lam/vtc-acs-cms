import FuseUtils from '@fuse/utils';
import FuseLoading from '@fuse/core/FuseLoading';
import { Navigate } from 'react-router-dom';
import settingsConfig from 'app/configs/settingsConfig';
import LoginConfig from '../main/login/LoginConfig';
import LogoutConfig from '../main/logout/LogoutConfig';
import Error404Page from '../main/404/Error404Page';
import IdentityConfig from '../main/identity/IdentityConfig';
import AccessReportConfig from '../main/access-report/AccessReportConfig';
import ReaderConfig from '../main/reader/ReaderConfig';
import GateSchedulerConfig from '../main/gate-scheduler/GateSchedulerConfig';
import DataSynchronizationConfig from '../main/data-synchronization/DataSynchronizationConfig';
import CustomReservedFieldsConfig from '../main/custom-reserved-fields/CustomReservedFieldsConfig';
import AccountConfig from '../main/account/AccountConfig';

const routeConfigs = [
  IdentityConfig,
  AccessReportConfig,
  ReaderConfig,
  GateSchedulerConfig,
  DataSynchronizationConfig,
  CustomReservedFieldsConfig,
  AccountConfig,
  LogoutConfig,
  LoginConfig,
];

const routes = [
  ...FuseUtils.generateRoutesFromConfigs(routeConfigs, settingsConfig.defaultAuth),
  {
    path: '/',
    element: <Navigate to="/identity/list" />,
    auth: settingsConfig.defaultAuth,
  },
  {
    path: 'loading',
    element: <FuseLoading />,
  },
  {
    path: '404',
    settings: {
      layout: {
        config: {
          navbar: {
            display: false,
          },
          toolbar: {
            display: false,
          },
          footer: {
            display: false,
          },
          leftSidePanel: {
            display: false,
          },
          rightSidePanel: {
            display: false,
          },
        },
      },
    },
    element: <Error404Page />,
  },
  {
    path: '*',
    element: <Navigate to="404" />,
  },
];

export default routes;
