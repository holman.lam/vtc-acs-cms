import { styled } from '@mui/material/styles';
import MuiCard from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import { openDialog } from 'app/store/fuse/dialogSlice';
import SchedulerForm from './SchedulerForm';

const Card = styled(MuiCard)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    boxShadow: 'none',
    background: 'none',
  },
}));

function LeftSidebar(props) {
  const dispatch = useDispatch();

  const handleAddScheduler = () => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <SchedulerForm />,
      })
    );
  };

  return (
    <div>
      <Card className="m-12">
        <CardContent className="flex flex-col items-center">
          <Button
            variant="contained"
            color="primary"
            fullWidth
            className="mb-24"
            onClick={handleAddScheduler}
          >
            Add Scheduler
          </Button>
        </CardContent>
      </Card>
    </div>
  );
}

export default LeftSidebar;
