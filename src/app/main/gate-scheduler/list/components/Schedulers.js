import { useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import MuiCard from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import { openDialog } from 'app/store/fuse/dialogSlice';
import SchedulerForm from './SchedulerForm';
import { Typography } from '@mui/material';
import { getSchedulerGate, getSchedulerDB } from '@api';
import { convertApiDateTimeToDisplayDate } from 'app/utils/date';

const Card = styled(MuiCard)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    boxShadow: 'none',
    background: 'none',
  },
}));

function Schedulers(props) {
  const dispatch = useDispatch();
  const [data, setData] = useState([]);

  const handleEditScheduler = (id) => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <SchedulerForm id={id}/>,
      })
    );
  };

  const convertDayToWeekend = (day) => {
    const weekdays = {
      monday: "MON",
      tuesday: "TUE",
      wednesday: "WED",
      thursday: "THU",
      friday: "FRI",
      saturday: "SAT",
      sunday: "SUN",
      // publicHoliday: "PUB",
    };
    let weekday = "";
    Object.keys(weekdays).map(function(k, i) {
      if(day == weekdays[k]){
        weekday = k;
      }
    });

    return weekday.charAt(0).toUpperCase() + weekday.slice(1).toLowerCase();
  }
  useEffect(() => {
    getSchedulerGate({ }).then((response) => {
      if (response.ok) {
        setData(response.data);
      }
    });
    getSchedulerDB({ }).then((response) => {
      if (response.ok) {
        console.log("getSchedulerDB");
        console.log(response.data);
      }
    });
  },[]);
  
  

  function renderItem(row) {
    return (
      <Grid item md={6} xs={12}>
        <Card className="m-12">
          <CardContent className="min-h-320 flex flex-col">
            <div className="flex-1">
              <Typography variant="subtitle2">{row.name}</Typography>

              {/* <div className="flex my-12 items-center">
                <img className="w-16 h-16 mr-12" src="assets/images/icons/type.svg" alt="Type" />

                <Typography>{row.title}</Typography>
              </div> */}

              <div className="flex my-12 items-center">
                <img
                  className="w-16 h-16 mr-12"
                  src="assets/images/icons/calendar.svg"
                  alt="Type"
                />

                <Typography>
                  {`${convertApiDateTimeToDisplayDate(
                    row.effectiveDate
                  )} to ${convertApiDateTimeToDisplayDate(row.expiryDate)}`}
                </Typography>
              </div>

              {row.scheduleList.map((s) => (
                <div className="flex my-12 items-center">
                  <img className="w-16 h-16 mr-12" src="assets/images/icons/time.svg" alt="Type" />

                  <Typography>{convertDayToWeekend(s.day)} {`${s.fromTime} to ${s.toTime}`}</Typography>
                  {/* TODO: gather all weekdays with the same time periods into one*/}
                </div>
              ))}
            </div>
            <div>
              <Button className="min-w-96" onClick={() => handleEditScheduler(row.scheduleId)}>
                Edit
              </Button>
            </div>
          </CardContent>
        </Card>
      </Grid>
    );
  }

  return <Grid container>{data.map(renderItem)}</Grid>;
}

export default Schedulers;
