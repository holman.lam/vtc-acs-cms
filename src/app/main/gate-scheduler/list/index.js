import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import LeftSidebar from './components/LeftSidebar';
import Schedulers from './components/Schedulers';

function Page() {
  return (
    <FusePageCarded
      header={<Header />}
      leftSidebarOpen
      leftSidebarContent={<LeftSidebar />}
      content={<Schedulers />}
      scroll="content"
    />
  );
}

export default Page;
