import GateScheduler from './list';

const GateSchedulerConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'gate-scheduler',
      element: <GateScheduler />,
    },
  ],
};

export default GateSchedulerConfig;
