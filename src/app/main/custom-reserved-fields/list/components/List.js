import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Switch from '@mui/material/Switch';
import { Typography } from '@mui/material';
import Stack from '@mui/material/Stack';
import { useDispatch } from 'react-redux';
import { openDialog } from 'app/store/fuse/dialogSlice';
import CustomReservedFieldForm from './CustomReservedFieldForm';
import useCutomReservedFields from './useCutomReservedFields';

const AntSwitch = styled(Switch)(({ theme }) => ({
  width: 56,
  height: 32,
  padding: 0,
  display: 'flex',
  '&:active': {
    '& .MuiSwitch-thumb': {
      width: 30,
    },
    '& .MuiSwitch-switchBase.Mui-checked': {
      transform: 'translateX(18px)',
    },
  },
  '& .MuiSwitch-switchBase': {
    padding: 4,
    '&.Mui-checked': {
      transform: 'translateX(24px)',
      color: '#fff',
      '& + .MuiSwitch-track': {
        opacity: 1,
        backgroundColor: '#0CC33D',
      },
    },
  },
  '& .MuiSwitch-thumb': {
    boxShadow: '0 2px 4px 0 rgb(0 35 11 / 20%)',
    width: 24,
    height: 24,
    borderRadius: 12,
    transition: theme.transitions.create(['width'], {
      duration: 200,
    }),
  },
  '& .MuiSwitch-track': {
    borderRadius: 32 / 2,
    opacity: 1,
    backgroundColor: theme.palette.mode === 'dark' ? 'rgba(255,255,255,.35)' : 'rgba(0,0,0,.25)',
    boxSizing: 'border-box',
  },
}));

function List(props) {
  const dispatch = useDispatch();
  const { data } = useCutomReservedFields();

  const handleEditCustomReservedField = (id) => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <CustomReservedFieldForm id={id} />,
      })
    );
  };

  return (
    <div className="flex flex-row flex-1 w-full space-y-8 sm:space-y-0 items-center justify-between py-12 lg:py-32 lg:px-32">
      <Grid container>
        <Grid item md={8} xs={12}>
          <Card className="m-12">
            <CardContent className="flex flex-row items-end">
              <div className="flex-1">
                <div className="flex items-center justify-between">
                  <Typography variant="subtitle2">CWA</Typography>

                  <Stack direction="row" spacing={1} alignItems="center">
                    <Typography>Off</Typography>
                    <AntSwitch defaultChecked />
                    <Typography>On</Typography>
                  </Stack>
                </div>

                <div className="flex my-12 items-center">
                  <img
                    className="w-16 h-16 mr-12"
                    src="assets/images/icons/field.svg"
                    alt="Field"
                  />

                  <Typography>Vaccine Pass</Typography>
                </div>

                <div className="flex my-12 items-center">
                  <img className="w-16 h-16 mr-12" src="assets/images/icons/type.svg" alt="Type" />

                  <Typography>Y, Image, Sound</Typography>
                </div>

                <Button className="mt-16 min-w-96" onClick={handleEditCustomReservedField}>
                  Edit
                </Button>
              </div>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

export default List;
