import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Toolbar from '@mui/material/Toolbar';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import _ from 'lodash';

function CustomReservedFieldForm({ id }) {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);

  const formik = useFormik({
    initialValues: {
      id: undefined,
    },
    validateOnMount: true,
    validationSchema: Yup.object({
      monday: Yup.object(),
      tuesday: Yup.object(),
      wednesday: Yup.object(),
      thursday: Yup.object(),
      friday: Yup.object(),
      saturday: Yup.object(),
      sunday: Yup.object(),
    }),
    onSubmit: async (values, helpers) => {},
  });

  return (
    <>
      <AppBar position="static" color="secondary" elevation={0}>
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            {id ? 'Edit' : 'New'} CWA
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        {loading ? (
          <div className="flex items-center justify-center h-full">
            <FuseLoading />
          </div>
        ) : (
          <>
            <div className="flex flex-row items-center">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/reserved.svg"
                alt="Calendar"
              />

              <Typography>Reserved</Typography>
            </div>

            <div className="flex flex-col mb-24">
              {['Y', 'N', '0', '1', '2', '3', '4', '5'].map((option) => {
                return (
                  <Grid sx={{ mb: 2 }}>
                    <Grid>
                      <FormControlLabel control={<Checkbox />} label={option} />
                    </Grid>

                    <div className="flex flex-row mb-24">
                      <img
                        className="w-20 h-20 mr-12 my-16"
                        src="assets/images/icons/field.svg"
                        alt="Field"
                      />

                      <TextField variant="outlined" fullWidth label="Title" name="value" />
                    </div>

                    <div className="flex flex-row mb-24">
                      <img
                        className="w-20 h-20 mr-12 my-16"
                        src="assets/images/icons/image.svg"
                        alt="Field"
                      />

                      <TextField variant="outlined" fullWidth label="Uplaod Image" name="image" />
                    </div>

                    <div className="flex flex-row mb-24">
                      <img
                        className="w-20 h-20 mr-12 my-16"
                        src="assets/images/icons/sound.svg"
                        alt="Field"
                      />

                      <TextField variant="outlined" fullWidth label="Uplaod Sound" name="sound" />
                    </div>
                  </Grid>
                );
              })}
            </div>
          </>
        )}
      </DialogContent>
      <DialogActions className="px-24 pb-24">
        <Button disabled={formik.isSubmitting} onClick={() => formik.handleSubmit()}>
          {id ? 'Save' : 'Add'}
        </Button>
      </DialogActions>
    </>
  );
}

export default CustomReservedFieldForm;
