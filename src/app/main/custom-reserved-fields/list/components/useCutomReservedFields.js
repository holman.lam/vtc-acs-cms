import { useEffect, useState } from 'react';
import { getCustomFields } from '@api';

const useCutomReservedFields = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchCustomFields = async () => {
      setLoading(true);
      const response = await getCustomFields();

      if (response.ok) {
        setData(response.data);
      }
      setLoading(false);
    };

    fetchCustomFields();
  }, []);

  return [
    {
      data,
      loading,
    },
  ];
};

export default useCutomReservedFields;
