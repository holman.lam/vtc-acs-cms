import CustomReservedFields from './list';

const CustomReservedFieldsConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'custom-reserved-fields',
      element: <CustomReservedFields />,
    },
  ],
};

export default CustomReservedFieldsConfig;
