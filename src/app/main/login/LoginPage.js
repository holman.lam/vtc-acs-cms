import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import LoginForm from './components/LoginForm';

function LoginPage() {
  return (
    <div className="flex-1 flex items-center justify-center">
      <Card className="m-24">
        <CardContent className="p-48 flex flex-col items-center">
          <img className="h-62 mb-24" src="assets/images/logo/logo-color.svg" alt="logo" />

          <Typography color="inherit" className="font-bold" textAlign="center">
            IVE(Chai Wan) Security Management System
          </Typography>

          <LoginForm />

          <Typography color="inherit">Or</Typography>

          <Button variant="contained" color="primary" className="mt-24 w-256 rounded" fullWidth>
            <img className="w-20 h-20 mr-12" src="assets/images/icons/adfs.svg" alt="ADFS" />
            Login with ADFS
          </Button>
        </CardContent>
      </Card>
    </div>
  );
}

export default LoginPage;
