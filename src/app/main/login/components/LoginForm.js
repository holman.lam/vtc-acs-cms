import { Button } from '@mui/material';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import PersonIcon from '@mui/icons-material/Person';
import LockIcon from '@mui/icons-material/Lock';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useAuth } from 'src/app/auth/AuthContext';

function LoginForm() {
  const { login } = useAuth();
  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validateOnMount: true,
    validationSchema: Yup.object({
      username: Yup.string().required().label('Login ID'),
      password: Yup.string().required().label('Password'),
    }),
    onSubmit: (values, helpers) => {
      login(values)
        .then((user) => {
          // No need to do anything, user data will be set at app/auth/AuthContext
        })
        .catch((_errors) => {
          helpers.setFieldError('password', 'Invalid username or password');
          helpers.setStatus({ success: false });
          helpers.setSubmitting(false);
        });
    },
  });

  return (
    <form
      className="my-24 max-w-xs flex flex-1 flex-col items-center w-full"
      onSubmit={formik.handleSubmit}
    >
      <TextField
        className="mb-24"
        autoFocus
        type="username"
        variant="outlined"
        fullWidth
        label="Login ID"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <PersonIcon />
            </InputAdornment>
          ),
        }}
        name="username"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        value={formik.values.username}
        helperText={
          Boolean(formik.touched.username && formik.errors.username) && formik.errors.username
        }
        error={Boolean(formik.touched.username && formik.errors.username)}
      />

      <TextField
        className="mb-24"
        variant="outlined"
        type="password"
        fullWidth
        label="Password"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <LockIcon />
            </InputAdornment>
          ),
        }}
        name="password"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        value={formik.values.password}
        helperText={
          Boolean(formik.touched.password && formik.errors.password) && formik.errors.password
        }
        error={Boolean(formik.touched.password && formik.errors.password)}
      />

      <Button
        type="submit"
        variant="contained"
        className="w-192"
        color="primary"
        disabled={!formik.isValid || formik.isSubmitting}
      >
        Login
      </Button>
    </form>
  );
}

export default LoginForm;
