import DataSynchronization from './list';

const DataSynchronizationConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'data-synchronization',
      element: <DataSynchronization />,
    },
  ],
};

export default DataSynchronizationConfig;
