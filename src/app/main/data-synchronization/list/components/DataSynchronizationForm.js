import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Toolbar from '@mui/material/Toolbar';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getReader, createReader, updateReader } from '@api';
import { Emitter, Event } from '@emitter';
import { useEffect, useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import { IconButton } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import _ from 'lodash';

const datLabels = {
  monday: 'Monday',
  tuesday: 'Tuesday',
  wednesday: 'Wednesday',
  thursday: 'Thursday',
  friday: 'Friday',
  saturday: 'Saturday',
  sunday: 'Sunday',
};

const dayInitValues = {
  enabled: false,
  times: [null],
};

function DataSynchronizationForm({ id }) {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);

  const formik = useFormik({
    initialValues: {
      id: undefined,
      monday: dayInitValues,
      tuesday: dayInitValues,
      wednesday: dayInitValues,
      thursday: dayInitValues,
      friday: dayInitValues,
      saturday: dayInitValues,
      sunday: dayInitValues,
    },
    validateOnMount: true,
    validationSchema: Yup.object({
      monday: Yup.object(),
      tuesday: Yup.object(),
      wednesday: Yup.object(),
      thursday: Yup.object(),
      friday: Yup.object(),
      saturday: Yup.object(),
      sunday: Yup.object(),
    }),
    onSubmit: async (values, helpers) => {
      if (values.id) {
        await handleUpdateRecord(values, helpers);
      } else {
        await handleCreateRecord(values, helpers);
      }
    },
  });

  async function handleCreateRecord(data, helpers) {}

  async function handleUpdateRecord(data, helpers) {}

  // If there is an id in the props,  need to get the record through the api
  useEffect(() => {
    if (id) {
      getReader({ id }).then((response) => {
        if (response.ok) {
          formik.setValues({
            id: response.data.id,
            name: response.data.name,
            locationRemark: response.data.locationRemark,
            type: response.data.type,
          });
        }
        setLoading(false);
      });
    } else {
      setLoading(false);
    }
  }, [id]);

  return (
    <>
      <AppBar position="static" color="secondary" elevation={0}>
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            {id ? 'Edit' : 'New'} Schedule
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        {loading ? (
          <div className="flex items-center justify-center h-full">
            <FuseLoading />
          </div>
        ) : (
          <>
            <div className="flex flex-row items-center">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/time.svg"
                alt="Calendar"
              />

              <Typography>Database Synchronization</Typography>
            </div>

            <div className="flex flex-col mb-24 pl-32">
              {['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'].map(
                (day) => {
                  const dayValues = formik.values[day];

                  return (
                    <Grid sx={{ mb: 2 }}>
                      <Grid>
                        <FormControlLabel
                          control={
                            <Checkbox
                              checked={dayValues.enabled}
                              onChange={formik.handleChange}
                              name={`${day}.enabled`}
                            />
                          }
                          label={datLabels[day]}
                        />
                      </Grid>

                      {dayValues.times.map((time, index) => {
                        return (
                          <Grid container className="items-center">
                            <TimePicker
                              ampm={false}
                              value={time}
                              onChange={(newValue) => {
                                formik.setFieldValue(`${day}.enabled[${index}]`, newValue);
                              }}
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  error={Boolean(
                                    _.get(formik.touched, `${day}.enabled[${index}]`) &&
                                      _.get(formik.errors, `${day}.enabled[${index}]`)
                                  )}
                                  helperText={
                                    Boolean(
                                      _.get(formik.touched, `${day}.enabled[${index}]`) &&
                                        _.get(formik.errors, `${day}.enabled[${index}]`)
                                    ) && _.get(formik.errors, `${day}.enabled[${index}]`)
                                  }
                                  size="small"
                                  sx={{ flex: 1 / 3 }}
                                />
                              )}
                            />

                            <IconButton size="small">
                              <AddCircleIcon fontSize="10" />
                            </IconButton>
                          </Grid>
                        );
                      })}
                    </Grid>
                  );
                }
              )}
            </div>
          </>
        )}
      </DialogContent>
      <DialogActions className="px-24 pb-24">
        <Button disabled={formik.isSubmitting} onClick={() => formik.handleSubmit()}>
          {id ? 'Save' : 'Add'}
        </Button>
      </DialogActions>
    </>
  );
}

export default DataSynchronizationForm;
