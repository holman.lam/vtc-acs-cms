import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import { Typography } from '@mui/material';
import { useDispatch } from 'react-redux';
import { openDialog } from 'app/store/fuse/dialogSlice';
import DataSynchronizationForm from './DataSynchronizationForm';

function Schedulers(props) {
  const dispatch = useDispatch();

  const handleEditSchedule = (id) => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <DataSynchronizationForm id={id} />,
      })
    );
  };

  return (
    <div className="flex flex-row flex-1 w-full space-y-8 sm:space-y-0 items-center justify-between py-12 lg:py-32 lg:px-32">
      <Grid container>
        <Grid item md={8} xs={12}>
          <Card className="m-12">
            <CardContent className="flex flex-row items-end">
              <div className="flex-1">
                <Typography variant="subtitle2">Database Synchronization Schedule</Typography>

                <div className="flex my-12 items-center">
                  <img
                    className="w-16 h-16 mr-12"
                    src="assets/images/icons/calendar.svg"
                    alt="Type"
                  />

                  <Typography>Tuesday & Thursday, 12:00</Typography>
                </div>
              </div>
              <div>
                <Button className="min-w-96" onClick={handleEditSchedule}>
                  Edit
                </Button>
              </div>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}

export default Schedulers;
