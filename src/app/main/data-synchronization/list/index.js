import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import List from './components/List';

function Page() {
  return <FusePageCarded header={<Header />} content={<List />} scroll="content" />;
}

export default Page;
