import IdentitiesList from './list';

const AccessReportConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: '/access-report',
      element: <IdentitiesList />,
    },
  ],
};

export default AccessReportConfig;
