import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import LeftSidebar from './components/LeftSidebar';
import Table from './components/AccessReportTable';
import { AccessReportTableProvider } from './components/AccessReportTable.context';

function Page() {
  return (
    <AccessReportTableProvider>
      <FusePageCarded
        header={<Header />}
        leftSidebarOpen
        leftSidebarContent={<LeftSidebar />}
        content={<Table />}
        scroll="content"
      />
    </AccessReportTableProvider>
  );
}

export default Page;
