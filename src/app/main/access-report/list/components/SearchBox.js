import Input from '@mui/material/Input';
import Paper from '@mui/material/Paper';
import { motion } from 'framer-motion';
import FuseSvgIcon from '@fuse/core/FuseSvgIcon';
import { useAccessReportTable } from './AccessReportTable.context';

function SearchBox(props) {
  const { searchValue, setSearchValue } = useAccessReportTable();

  return (
    <div className="flex flex-1 flex-col items-center space-x-8 w-full sm:w-auto">
      <Paper
        component={motion.div}
        initial={{ y: -20, opacity: 0 }}
        animate={{ y: 0, opacity: 1, transition: { delay: 0.2 } }}
        className="flex items-center w-full lg:max-w-480 space-x-8 px-16 rounded-md border-1 shadow-0"
      >
        <FuseSvgIcon color="disabled">heroicons-solid:search</FuseSvgIcon>

        <Input
          placeholder="Search"
          className="flex flex-1"
          disableUnderline
          fullWidth
          inputProps={{
            'aria-label': 'Search',
          }}
          value={searchValue}
          onChange={(e) => setSearchValue(e.target.value)}
        />
      </Paper>
    </div>
  );
}

export default SearchBox;
