import { createContext, useState, useContext } from 'react';

export const AccessReportTableContext = createContext({
  searchValue: '',
  setSearchValue: () => {},
});

export const AccessReportTableProvider = (props) => {
  const { children } = props;
  const [searchValue, setSearchValue] = useState('');

  return (
    <AccessReportTableContext.Provider
      value={{
        searchValue,
        setSearchValue,
      }}
    >
      {children}
    </AccessReportTableContext.Provider>
  );
};

export const AccessReportTableConsumer = AccessReportTableContext.Consumer;
export const useAccessReportTable = () => useContext(AccessReportTableContext);
