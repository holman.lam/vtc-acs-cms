import { useEffect, useRef } from 'react';
import Table from 'app/shared-components/Table';
import { useDebounce } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { getAccessLogs } from '@api';
import { convertApiDateTimeToDisplayDateTime, extractDateFromDateTime, extractTimeFromDateTime } from 'app/utils/date';
import { useAccessReportTable } from './AccessReportTable.context';

const columns = [
  { id: 'accessDateTime', label: 'Date', minWidth: 170, render: ({ rowData }) => extractDateFromDateTime(rowData.dateOfEntry),},
  { id: 'time', label: 'Time', minWidth: 170, render: ({ rowData }) => extractTimeFromDateTime(rowData.dateOfEntry),},
  { id: 'reader', label: 'Reader', minWidth: 170 },
  { id: 'status', label: 'Status', minWidth: 170 },
  { id: 'remark', label: 'Reason', minWidth: 170 },
  { id: 'cna', label: 'CNA', minWidth: 170 },
  { id: 'smartCardId', label: 'Smart Card ID.', minWidth: 170 },
  { id: 'firstName', label: 'First Name', minWidth: 150 },
  { id: 'lastName', label: 'Last Name', minWidth: 150 },
  { id: 'chineseName', label: 'Chinese Name', minWidth: 170 },
  { id: 'department', label: 'Department', minWidth: 100 },
  { id: 'accountType', label: 'Account Type', minWidth: 100 },
  // { id: 'courseNo', label: 'Course No.', minWidth: 100 },
  // { id: 'module', label: 'Module', minWidth: 100 },
  // { id: 'year', label: 'Year', minWidth: 100 },
  // { id: 'class', label: 'Class', minWidth: 100 },
  // { id: 'site', label: 'Site', minWidth: 100 },
  // {
  //   id: 'dateOfEntry',
  //   label: 'Date of entry',
  //   minWidth: 150,
  //   render: ({ rowData }) => convertApiDateTimeToDisplayDateTime(rowData.dateOfEntry),
  // },
  // {
  //   id: 'expiryDate',
  //   label: 'Expiry date',
  //   minWidth: 150,
  //   render: ({ rowData }) => convertApiDateTimeToDisplayDateTime(rowData.expiryDate),
  // },
  // { id: 'buildingAccess', label: 'Building Access', minWidth: 170 },
  // { id: 'cwa', label: 'CWA', minWidth: 100 },
  // { id: 'cwa', label: 'CWB', minWidth: 100 },
  // { id: 'cwa', label: 'CWC', minWidth: 100 },
  // { id: 'cwa', label: 'CWE', minWidth: 100 },
  // { id: 'status', label: 'Status', minWidth: 170 },
  // { id: 'reason', label: 'Reason', minWidth: 170 },
];

function AccessReportTable(props) {
  const dispatch = useDispatch();
  const tableRef = useRef(null);
  const { searchValue } = useAccessReportTable();
  const debouncedsearchValue = useDebounce(searchValue, 500);

  function reloadTable() {
    tableRef.current.reload();
  }

  useEffect(() => {
    reloadTable();
  }, [debouncedsearchValue]);

  useEffect(() => {}, []);

  const dataSource = ({ page, pageSize, sort }) => {
    const data = {
      currentPage: page,
      pageSize,
      filterQuery: "",
      order: [
        {
          column: "id",
          dir: sort.direction,
        },
      ],
    };
    return new Promise((resolve, reject) => {
      getAccessLogs({
        data,
      }).then((response) => {
        if (response.ok) {
          return resolve({
            data: response.data.content,
            count: response.data.total,
          });
        }
        return reject(response?.data?.errorMessage || response.problem);
      });
    });
  };

  return (
    <Table
      ref={tableRef}
      columns={columns}
      dataSource={dataSource}
      defaultSort={{ direction: 'desc', id: 'date' }}
    />
  );
}

export default AccessReportTable;
