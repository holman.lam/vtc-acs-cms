import { styled } from '@mui/material/styles';
import MuiCard from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import { Divider, Hidden, Typography, Box, TextField } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import SearchBox from './SearchBox';

const Card = styled(MuiCard)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    boxShadow: 'none',
    background: 'none',
  },
}));

function LeftSidebar(props) {
  return (
    <div>
      <Card className="m-12">
        <CardContent className="flex flex-col items-center">
          <Button variant="contained" color="primary" fullWidth className="mb-12">
            Export to PDF
          </Button>

          <Button variant="contained" color="primary" fullWidth className="mb-12">
            Export to CSV
          </Button>

          <Divider flexItem className="mb-12" />

          <Box className="w-full">
            <Typography className="my-12 ">Date Range</Typography>

            <Typography>From</Typography>
            <DatePicker
              value={null}
              onChange={(newValue) => {}}
              renderInput={(params) => <TextField {...params} size="small" className="mb-12" />}
            />

            <Typography>To</Typography>
            <DatePicker
              value={null}
              onChange={(newValue) => {}}
              renderInput={(params) => <TextField {...params} size="small" className="mb-24" />}
            />

            <Button variant="contained" color="primary" fullWidth>
              Filter
            </Button>
          </Box>

          <Hidden lgUp>
            <SearchBox />
          </Hidden>
        </CardContent>
      </Card>
    </div>
  );
}

export default LeftSidebar;
