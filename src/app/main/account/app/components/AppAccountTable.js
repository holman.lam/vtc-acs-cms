import { useEffect, useRef } from 'react';
import { getAppAccounts } from '@api';
import Table from 'app/shared-components/Table';
import { Emitter, Event } from '@emitter';
import { useDebounce } from 'app/hooks';
import EditIcon from '@mui/icons-material/Edit';
import { useDispatch } from 'react-redux';
import { openDialog } from 'app/store/fuse/dialogSlice';
import { useAppAccountTable } from './AppAccountTable.context';
import AppAccountForm from './AppAccountForm';

const columns = [
  { id: 'username', label: 'Username', minWidth: 170 },
  {
    id: 'lastLoginTime',
    label: 'Last Login Time',
    minWidth: 170,
    render: ({ rowData }) => '-', // TODO: missing last login time from api.
  },
  {
    id: 'enabled',
    label: 'Status',
    render: ({ rowData }) => (rowData.enabled ? 'Enabled' : 'Disabled'),
  },
];

function AppAccountTable(props) {
  const dispatch = useDispatch();
  const tableRef = useRef(null);
  const { searchValue } = useAppAccountTable();
  const debouncedSearchValue = useDebounce(searchValue, 500);

  function reloadTable() {
    tableRef.current.reload();
  }

  useEffect(() => {
    reloadTable();
  }, [debouncedSearchValue]);

  useEffect(() => {
    Emitter.on(Event.APP_ACCOUNT_CREATED, reloadTable);
    Emitter.on(Event.APP_ACCOUNT_UPDATED, reloadTable);
    return () => {
      Emitter.off(Event.APP_ACCOUNT_CREATED, reloadTable);
      Emitter.off(Event.APP_ACCOUNT_UPDATED, reloadTable);
    };
  }, []);

  const dataSource = ({ page, pageSize, sort }) => {
    const data = {
      currentPage: page,
      pageSize,
      order: [
        {
          column: 0,
          dir: sort.direction,
        },
      ],
      columns: [
        // column 0 is used for sorting
        {
          data: sort.id,
          searchable: true,
          orderable: true,
          search: {
            value: '',
            regex: false,
          },
        },
        // for searching
        {
          data: 'username',
          searchable: true,
          orderable: true,
          search: {
            value: `ONE("${debouncedSearchValue}")`,
            regex: false,
          },
        },
      ],
    };
    return new Promise((resolve, reject) => {
      getAppAccounts({
        data,
      }).then((response) => {
        if (response.ok) {
          return resolve({
            data: response.data.content,
            count: response.data.total,
          });
        }
        return reject(response?.data?.errorMessage || response.problem);
      });
    });
  };

  const handleEditReader = (username) => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <AppAccountForm username={username} />,
      })
    );
  };

  return (
    <Table
      ref={tableRef}
      columns={columns}
      dataSource={dataSource}
      defaultSort={{ direction: 'asc', id: 'username' }}
      actions={[
        (rowData) => ({
          icon: <EditIcon />,
          tooltip: 'Edit',
          onClick: () => handleEditReader(rowData.username),
        }),
      ]}
    />
  );
}

export default AppAccountTable;
