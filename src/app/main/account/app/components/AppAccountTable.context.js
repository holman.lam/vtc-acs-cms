import { createContext, useState, useContext } from 'react';

export const AppAccountTableContext = createContext({
  searchValue: '',
  setSearchValue: () => {},
});

export const AppAccountTableProvider = (props) => {
  const { children } = props;
  const [searchValue, setSearchValue] = useState('');

  return (
    <AppAccountTableContext.Provider
      value={{
        searchValue,
        setSearchValue,
      }}
    >
      {children}
    </AppAccountTableContext.Provider>
  );
};

export const AppAccountTableConsumer = AppAccountTableContext.Consumer;
export const useAppAccountTable = () => useContext(AppAccountTableContext);
