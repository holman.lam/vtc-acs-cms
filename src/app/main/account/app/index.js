import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import LeftSidebar from './components/LeftSidebar';
import Table from './components/AppAccountTable';
import { AppAccountTableProvider } from './components/AppAccountTable.context';

function Page() {
  return (
    <AppAccountTableProvider>
      <FusePageCarded
        header={<Header />}
        leftSidebarOpen
        leftSidebarContent={<LeftSidebar />}
        content={<Table />}
        scroll="content"
      />
    </AppAccountTableProvider>
  );
}

export default Page;
