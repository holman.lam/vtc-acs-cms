import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Toolbar from '@mui/material/Toolbar';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { assignDepartment } from '@api';
import { Emitter, Event } from '@emitter';
import { closeDialog } from 'app/store/fuse/dialogSlice';
import useDepartments from 'app/hooks/useDepartments';

function AssignForm({ selectedIds }) {
  const dispatch = useDispatch();
  const [{ data: departments }] = useDepartments();

  const formik = useFormik({
    initialValues: {
      departmentId: undefined,
    },
    validateOnMount: true,
    validationSchema: Yup.object({
      departmentId: Yup.string().required().label('Department'),
    }),
    onSubmit: handleAssignDepartment,
  });

  async function handleAssignDepartment(data) {
    const { departmentId } = data;
    const res = await assignDepartment({
      data: {
        departmentId,
        users: selectedIds,
      },
    });
    if (res.ok) {
      Emitter.emit(Event.ASSIGNED_USER_DEPARTMENT, res.data);
      dispatch(closeDialog());
    }
  }

  return (
    <>
      <AppBar position="static" color="secondary" elevation={0}>
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            Assign to department
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        <div className="flex flex-row mb-24">
          <img
            className="w-20 h-20 mr-12 my-16"
            src="assets/images/icons/department.svg"
            alt="Department"
          />

          <FormControl
            fullWidth
            error={Boolean(formik.touched.departmentId && formik.errors.departmentId)}
          >
            <InputLabel id="demo-simple-select-helper-label">Department</InputLabel>
            <Select
              label="Department"
              id="department"
              name="departmentId"
              value={formik.values.departmentId || ''}
              onChange={formik.handleChange}
              error={Boolean(formik.touched.departmentId && formik.errors.departmentId)}
            >
              {departments.map((d) => (
                <MenuItem key={d.departmentId} name="departmentId" value={d.departmentId}>
                  {d.name}
                </MenuItem>
              ))}
            </Select>

            <FormHelperText>
              {Boolean(formik.touched.departmentId && formik.errors.departmentId) &&
                formik.errors.departmentId}
            </FormHelperText>
          </FormControl>
        </div>
      </DialogContent>
      <DialogActions className="px-24 pb-24">
        <Button
          disabled={!formik.isValid || formik.isSubmitting}
          onClick={() => formik.handleSubmit()}
        >
          Add
        </Button>
      </DialogActions>
    </>
  );
}

export default AssignForm;
