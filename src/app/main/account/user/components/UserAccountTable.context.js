import { createContext, useState, useContext } from 'react';

export const UserAccountTableContext = createContext({
  department: '',
  searchValue: '',
  setSearchValue: () => {},
});

export const UserAccountTableProvider = (props) => {
  const { children } = props;
  const [department, setDepartment] = useState(undefined);
  const [searchValue, setSearchValue] = useState('');

  return (
    <UserAccountTableContext.Provider
      value={{
        department,
        setDepartment,
        searchValue,
        setSearchValue,
      }}
    >
      {children}
    </UserAccountTableContext.Provider>
  );
};

export const UserTableConsumer = UserAccountTableContext.Consumer;
export const useUserTable = () => useContext(UserAccountTableContext);
