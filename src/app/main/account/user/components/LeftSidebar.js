import { styled } from '@mui/material/styles';
import MuiCard from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import { openDialog } from 'app/store/fuse/dialogSlice';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import GroupIcon from '@mui/icons-material/Group';
import CmsUserForm from './UserAccountForm';
import useDepartments from 'app/hooks/useDepartments';
import { useUserTable } from './UserAccountTable.context';

const Card = styled(MuiCard)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    boxShadow: 'none',
    background: 'none',
  },
}));

function LeftSidebar(props) {
  const dispatch = useDispatch();
  const [{ data: departments }] = useDepartments();
  const { department, setDepartment } = useUserTable();

  const handleAddReader = () => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <CmsUserForm />,
      })
    );
  };

  return (
    <div>
      <Card className="m-12">
        <CardContent className="flex flex-col items-center">
          <Button
            variant="contained"
            color="primary"
            fullWidth
            className="mb-24"
            onClick={handleAddReader}
          >
            Add Account
          </Button>

          <ListItem disablePadding>
            <ListItemButton
              href="#"
              selected={department === undefined}
              onClick={() => setDepartment(undefined)}
            >
              <ListItemIcon className="min-w-40">
                <GroupIcon />
              </ListItemIcon>
              <ListItemText primary="All User" />
            </ListItemButton>
          </ListItem>

          {departments.map((d) => (
            <ListItem disablePadding>
              <ListItemButton
                href="#"
                selected={department === d.departmentId}
                onClick={() => setDepartment(d.departmentId)}
              >
                <ListItemIcon className="min-w-40">
                  <GroupIcon />
                </ListItemIcon>
                <ListItemText primary={d.name} />
              </ListItemButton>
            </ListItem>
          ))}
        </CardContent>
      </Card>
    </div>
  );
}

export default LeftSidebar;
