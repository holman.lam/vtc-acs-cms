import { useEffect, useRef, useState } from 'react';
import { getUserAccounts } from '@api';
import Table from 'app/shared-components/Table';
import Box from '@mui/material/Box';
import { Typography, Button } from '@mui/material';
import { Emitter, Event } from '@emitter';
import EditIcon from '@mui/icons-material/Edit';
import { useDispatch } from 'react-redux';
import { openDialog } from 'app/store/fuse/dialogSlice';
import { useUserTable } from './UserAccountTable.context';
import UserForm from './UserAccountForm';
import AssignForm from './AssignForm';

const columns = [
  { id: 'username', label: 'Username', minWidth: 170 },
  { id: 'department', label: 'Department', minWidth: 170 },
  {
    id: 'lastLoginTime',
    label: 'Last Login Time',
    minWidth: 170,
    render: ({ rowData }) => '-', // TODO: missing last login time from api.
  },
  {
    id: 'enabled',
    label: 'Status',
    render: ({ rowData }) => (rowData.enabled ? 'Enabled' : 'Disabled'),
  },
];

function UserAccountTable(props) {
  const dispatch = useDispatch();
  const tableRef = useRef(null);
  const { department } = useUserTable();
  const [selected, setSelected] = useState([]);

  function reloadTable() {
    tableRef.current.reload();
  }

  function onAssigned() {
    setSelected([]);
    tableRef.current.reload();
  }

  useEffect(() => {
    reloadTable();
  }, [department]);

  useEffect(() => {
    Emitter.on(Event.USER_ACCOUNT_CREATED, reloadTable);
    Emitter.on(Event.USER_ACCOUNT_UPDATED, reloadTable);
    Emitter.on(Event.ASSIGNED_USER_DEPARTMENT, onAssigned);
    return () => {
      Emitter.off(Event.USER_ACCOUNT_CREATED);
      Emitter.off(Event.USER_ACCOUNT_UPDATED);
      Emitter.off(Event.ASSIGNED_USER_DEPARTMENT);
    };
  }, []);

  const dataSource = ({ page, pageSize, sort }) => {
    const data = {
      currentPage: page,
      pageSize,
      order: [
        {
          column: 0,
          dir: sort.direction,
        },
      ],
      columns: [
        // column 0 is used for sorting
        {
          data: sort.id,
          searchable: true,
          orderable: true,
          search: {
            value: '',
            regex: false,
          },
        },
      ],
    };

    if (department) {
      data.columns.push({
        data: 'department',
        searchable: true,
        orderable: true,
        search: {
          value: `ONE("${department}")`,
          regex: false,
        },
      });
    }
    return new Promise((resolve, reject) => {
      getUserAccounts({
        data,
      }).then((response) => {
        if (response.ok) {
          return resolve({
            data: response.data.content,
            count: response.data.total,
          });
        }
        return reject(response?.data?.errorMessage || response.problem);
      });
    });
  };

  const handleEditUserAccount = (username) => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <UserForm username={username} />,
      })
    );
  };

  const handleAssignDepartment = () => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <AssignForm selectedIds={selected} />,
      })
    );
  };

  const handleSelect = (row) => {
    const selectedIndex = selected.indexOf(row.username);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, row.username);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleSelectAllClick = (event, rows) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.username);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  function ActionBar() {
    return (
      <Box
        container
        className="flex items-center justify-center"
        sx={{
          backgroundColor: (theme) => theme.palette.grey[300],
          borderRadius: (theme) => theme.spacing(1),
          p: (theme) => theme.spacing(1),
        }}
      >
        <Typography
          fontWeight={600}
          color="black"
          sx={{ mr: 2 }}
        >{`${selected.length} results has been selected.`}</Typography>

        <Button variant="text" size="small" onClick={handleAssignDepartment}>
          Assign
        </Button>
      </Box>
    );
  }

  return (
    <Table
      ref={tableRef}
      columns={columns}
      dataSource={dataSource}
      defaultSort={{ direction: 'asc', id: 'username' }}
      actions={[
        (rowData) => ({
          icon: <EditIcon />,
          tooltip: 'Edit',
          onClick: () => handleEditUserAccount(rowData.username),
        }),
      ]}
      selection
      selected={selected}
      isSelected={(row) => selected.indexOf(row.username) >= 0}
      onSelect={handleSelect}
      onSelectAll={handleSelectAllClick}
      actionBar={selected.length > 0 ? <ActionBar /> : undefined}
    />
  );
}

export default UserAccountTable;
