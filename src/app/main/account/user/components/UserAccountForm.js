import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Toolbar from '@mui/material/Toolbar';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import { createUserAccount, updateUserAccount, getUserAccount } from '@api';
import { Emitter, Event } from '@emitter';
import { closeDialog } from 'app/store/fuse/dialogSlice';

function UserAccountForm({ username }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);

  const formik = useFormik({
    initialValues: {
      id: undefined,
      username: '',
      password: '',
      repeatPassword: '',
      isEnabled: true,
    },
    validateOnMount: true,
    validationSchema: Yup.object({
      username: Yup.string().required().label('Username'),
      password: Yup.string()
        .when(['id'], {
          is: (id) => id !== undefined,
          then: Yup.string(),
          otherwise: Yup.string().required(),
        })
        .label('Password'),
      repeatPassword: Yup.string()
        .label('Confirm Password')
        .oneOf([Yup.ref('password'), null], 'Passwords must match')
        .when(['id'], {
          is: (id) => id !== undefined,
          then: Yup.string(),
          otherwise: Yup.string().required(),
        }),
    }),
    onSubmit: async (values, helpers) => {
      if (values.id) {
        await handleUpdateRecord(values, helpers);
      } else {
        await handleCreateRecord(values, helpers);
      }
    },
  });

  async function handleCreateRecord(data, helpers) {
    const { isEnabled, ...rest } = data;
    const res = await createUserAccount({
      data: {
        ...rest,
        departmentId: 1,
        isEnabled,
      },
    });
    if (res.ok) {
      Emitter.emit(Event.USER_ACCOUNT_CREATED, res.data);
      dispatch(closeDialog());
    }
  }

  async function handleUpdateRecord(data, helpers) {
    const { isEnabled, ...rest } = data;
    const res = await updateUserAccount({
      username: data.id,
      data: {
        ...rest,
        enabled: isEnabled,
      },
    });
    if (res.ok) {
      Emitter.emit(Event.APP_ACCOUNT_UPDATED, res.data);
      dispatch(closeDialog());
    }

    // TODO: edit user not working
  }

  // If there is an id in the props, need to get the record through the api
  useEffect(() => {
    if (username) {
      getUserAccount({ username }).then((response) => {
        if (response.ok) {
          formik.setValues({
            id: response.data.username,
            username: response.data.username,
            password: '',
            repeatPassword: '',
            isEnabled: response.data.enabled,
          });
        }
        setLoading(false);
      });
    } else {
      setLoading(false);
    }
  }, [username]);

  return (
    <>
      <AppBar position="static" color="secondary" elevation={0}>
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            {username ? 'Edit' : 'New'} User
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        {loading ? (
          <div className="flex items-center justify-center h-full">
            <FuseLoading />
          </div>
        ) : (
          <div className="my-24">
            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/username.svg"
                alt="Username"
              />

              <TextField
                autoFocus
                variant="outlined"
                fullWidth
                label="Username"
                name="username"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.username}
                helperText={
                  Boolean(formik.touched.username && formik.errors.username) && formik.errors.username
                }
                error={Boolean(formik.touched.username && formik.errors.username)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/password.svg"
                alt="Password"
              />

              <TextField
                variant="outlined"
                fullWidth
                label="Password"
                name="password"
                type="password"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.password}
                helperText={
                  Boolean(formik.touched.password && formik.errors.password) &&
                  formik.errors.password
                }
                error={Boolean(formik.touched.password && formik.errors.password)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/password.svg"
                alt="Password"
              />

              <TextField
                variant="outlined"
                fullWidth
                label="Confirm Password"
                name="repeatPassword"
                type="password"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.repeatPassword}
                helperText={
                  Boolean(formik.touched.repeatPassword && formik.errors.repeatPassword) &&
                  formik.errors.repeatPassword
                }
                error={Boolean(formik.touched.repeatPassword && formik.errors.repeatPassword)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/enabled.svg"
                alt="Enabled"
              />
              <FormControl fullWidth>
                <Select
                  id="isEnabled"
                  name="isEnabled"
                  value={formik.values.isEnabled}
                  onChange={formik.handleChange}
                  error={Boolean(formik.touched.isEnabled && formik.errors.isEnabled)}
                >
                  <MenuItem name="isEnabled" value>
                    Enabled
                  </MenuItem>
                  <MenuItem name="isEnabled" value={false}>
                    Disabled
                  </MenuItem>
                </Select>
              </FormControl>
            </div>
          </div>
        )}
      </DialogContent>
      <DialogActions className="px-24 pb-24">
        <Button disabled={formik.isSubmitting} onClick={() => formik.handleSubmit()}>
          {username ? 'Save' : 'Add'}
        </Button>
      </DialogActions>
    </>
  );
}

export default UserAccountForm;
