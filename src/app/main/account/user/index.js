import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import LeftSidebar from './components/LeftSidebar';
import Table from './components/UserAccountTable';
import { UserAccountTableProvider } from './components/UserAccountTable.context';

function Page() {
  return (
    <UserAccountTableProvider>
      <FusePageCarded
        header={<Header />}
        leftSidebarOpen
        leftSidebarContent={<LeftSidebar />}
        content={<Table />}
        scroll="content"
      />
    </UserAccountTableProvider>
  );
}

export default Page;
