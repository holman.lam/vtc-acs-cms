import AppAccounts from './app';
import UserAccounts from './user';

const AccountConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'account/user',
      element: <UserAccounts />,
    },
    {
      path: 'account/app',
      element: <AppAccounts />,
    },
  ],
};

export default AccountConfig;
