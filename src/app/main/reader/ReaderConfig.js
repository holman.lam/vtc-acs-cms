import Readers from './readers';

const ReaderConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'reader',
      element: <Readers />,
    },
  ],
};

export default ReaderConfig;
