import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Toolbar from '@mui/material/Toolbar';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { getReader, createReader, updateReader } from '@api';
import { Emitter, Event } from '@emitter';
import { useEffect, useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import { closeDialog } from 'app/store/fuse/dialogSlice';
import useLocations from 'app/hooks/useLocations';

function ReaderForm({ id }) {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);
  const [{ data: locations }] = useLocations();

  const formik = useFormik({
    initialValues: {
      id: undefined,
      name: '',
      locationId: '',
      type: '',
    },
    validateOnMount: true,
    validationSchema: Yup.object({
      name: Yup.string().required().label('Reader Name'),
      locationId: Yup.string().required().label('Location'),
      type: Yup.string().required().label('Type'),
    }),
    onSubmit: async (values, helpers) => {
      if (values.id) {
        await handleUpdateRecord(values, helpers);
      } else {
        await handleCreateRecord(values, helpers);
      }
    },
  });

  async function handleCreateRecord(data, helpers) {
    const res = await createReader({ data });
    if (res.ok) {
      Emitter.emit(Event.READER_CREATED, res.data);
      dispatch(closeDialog());
    }
  }

  async function handleUpdateRecord(data, helpers) {
    const res = await updateReader({ data });
    if (res.ok) {
      Emitter.emit(Event.READER_UPDATED, res.data);
      dispatch(closeDialog());
    }
  }

  // If there is an id in the props,  need to get the record through the api
  useEffect(() => {
    if (id) {
      getReader({ id }).then((response) => {
        if (response.ok) {
          formik.setValues({
            id: response.data.id,
            name: response.data.name,
            locationId: response.data.location?.id,
            type: response.data.type,
          });
        }
        setLoading(false);
      });
    } else {
      setLoading(false);
    }
  }, [id]);

  return (
    <>
      <AppBar position="static" color="secondary" elevation={0}>
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            {id ? 'Edit' : 'New'} Reader
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        {loading ? (
          <div className="flex items-center justify-center h-full">
            <FuseLoading />
          </div>
        ) : (
          <div className="my-24">
            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/name.svg"
                alt="Name"
              />

              <TextField
                autoFocus
                variant="outlined"
                fullWidth
                label="Reader Name"
                name="name"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.name}
                helperText={
                  Boolean(formik.touched.name && formik.errors.name) && formik.errors.name
                }
                error={Boolean(formik.touched.name && formik.errors.name)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/location.svg"
                alt="Location"
              />

              <FormControl
                fullWidth
                error={Boolean(formik.touched.locationId && formik.errors.locationId)}
              >
                <InputLabel>Location</InputLabel>
                <Select
                  label="Location"
                  name="locationId"
                  value={formik.values.locationId || ''}
                  onChange={formik.handleChange}
                  error={Boolean(formik.touched.locationId && formik.errors.locationId)}
                >
                  {locations.map((l) => (
                    <MenuItem key={l.id} value={l.id}>
                      {l.name}
                    </MenuItem>
                  ))}
                </Select>

                <FormHelperText>
                  {Boolean(formik.touched.locationId && formik.errors.locationId) &&
                    formik.errors.locationId}
                </FormHelperText>
              </FormControl>
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/type.svg"
                alt="Type"
              />

              <TextField
                variant="outlined"
                fullWidth
                label="Type"
                name="type"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.type}
                helperText={
                  Boolean(formik.touched.type && formik.errors.type) && formik.errors.type
                }
                error={Boolean(formik.touched.type && formik.errors.type)}
              />
            </div>
          </div>
        )}
      </DialogContent>
      <DialogActions className="px-24 pb-24">
        <Button disabled={formik.isSubmitting} onClick={() => formik.handleSubmit()}>
          {id ? 'Save' : 'Add'}
        </Button>
      </DialogActions>
    </>
  );
}

export default ReaderForm;
