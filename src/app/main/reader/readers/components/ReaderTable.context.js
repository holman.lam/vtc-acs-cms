import { createContext, useState, useContext } from 'react';

export const ReaderTableContext = createContext({
  searchValue: '',
  setSearchValue: () => {},
});

export const ReaderTableProvider = (props) => {
  const { children } = props;
  const [searchValue, setSearchValue] = useState('');

  return (
    <ReaderTableContext.Provider
      value={{
        searchValue,
        setSearchValue,
      }}
    >
      {children}
    </ReaderTableContext.Provider>
  );
};

export const ReaderTableConsumer = ReaderTableContext.Consumer;
export const useReaderTable = () => useContext(ReaderTableContext);
