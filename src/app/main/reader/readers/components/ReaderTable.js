import { useEffect, useRef } from 'react';
import { getReaders } from '@api';
import Table from 'app/shared-components/Table';
import { Emitter, Event } from '@emitter';
import { useDebounce } from 'app/hooks';
import EditIcon from '@mui/icons-material/Edit';
import { useDispatch } from 'react-redux';
import { openDialog } from 'app/store/fuse/dialogSlice';
import { convertApiDateTimeToDisplayDateTime } from 'app/utils/date';
import { useReaderTable } from './ReaderTable.context';
import ReaderForm from './ReaderForm';

const columns = [
  { id: 'name', label: 'Name', minWidth: 170 },
  { id: 'location.name', label: 'Location', minWidth: 170 },
  {
    id: 'type',
    label: 'Type',
    minWidth: 100,
  },
  {
    id: 'lastSyncUp',
    label: 'Last Sync (Up)',
  },
  {
    id: 'lastUpdateTime',
    label: 'Last Sync (Down)',
    sorting: false,
    render: ({ rowData }) => convertApiDateTimeToDisplayDateTime(rowData.lastUpdateTime),
  },
];

function ReaderTable(props) {
  const dispatch = useDispatch();
  const tableRef = useRef(null);
  const { searchValue } = useReaderTable();
  const debouncedsearchValue = useDebounce(searchValue, 500);

  function reloadTable() {
    tableRef.current.reload();
  }

  useEffect(() => {
    reloadTable();
  }, [debouncedsearchValue]);

  useEffect(() => {
    Emitter.on(Event.READER_CREATED, reloadTable);
    Emitter.on(Event.READER_UPDATED, reloadTable);
    return () => {
      Emitter.off(Event.READER_CREATED, reloadTable);
      Emitter.off(Event.READER_UPDATED, reloadTable);
    };
  }, []);

  const dataSource = ({ page, pageSize, sort }) => {
    const data = {
      currentPage: page,
      pageSize,
      filterQuery: "",
      order: [
        {
          column: sort.id,
          dir: sort.direction,
        },
      ],
    };
    return new Promise((resolve, reject) => {
      getReaders({
        data,
      }).then((response) => {
        if (response.ok) {
          return resolve({
            data: response.data.content,
            count: response.data.total,
          });
        }
        return reject(response?.data?.errorMessage || response.problem);
      });
    });
  };

  const handleEditReader = (id) => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <ReaderForm id={id} />,
      })
    );
  };

  return (
    <Table
      ref={tableRef}
      columns={columns}
      dataSource={dataSource}
      defaultSort={{ direction: 'asc', id: 'name' }}
      actions={[
        (rowData) => ({
          icon: <EditIcon />,
          tooltip: 'Edit',
          onClick: () => handleEditReader(rowData.id),
        }),
      ]}
    />
  );
}

export default ReaderTable;
