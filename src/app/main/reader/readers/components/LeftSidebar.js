import { styled } from '@mui/material/styles';
import MuiCard from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import { openDialog } from 'app/store/fuse/dialogSlice';
import { Hidden } from '@mui/material';
import SearchBox from './SearchBox';
import ReaderForm from './ReaderForm';

const Card = styled(MuiCard)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    boxShadow: 'none',
    background: 'none',
  },
}));

function LeftSidebar(props) {
  const dispatch = useDispatch();

  const handleAddReader = () => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <ReaderForm />,
      })
    );
  };

  return (
    <div>
      <Card className="m-12">
        <CardContent className="flex flex-col items-center">
          <Button
            variant="contained"
            color="primary"
            fullWidth
            className="mb-24"
            onClick={handleAddReader}
          >
            Add Reader
          </Button>

          <Hidden lgUp>
            <SearchBox />
          </Hidden>
        </CardContent>
      </Card>
    </div>
  );
}

export default LeftSidebar;
