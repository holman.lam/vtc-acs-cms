import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import LeftSidebar from './components/LeftSidebar';
import Table from './components/ReaderTable';
import { ReaderTableProvider } from './components/ReaderTable.context';

function Page() {
  return (
    <ReaderTableProvider>
      <FusePageCarded
        header={<Header />}
        leftSidebarOpen
        leftSidebarContent={<LeftSidebar />}
        content={<Table />}
        scroll="content"
      />
    </ReaderTableProvider>
  );
}

export default Page;
