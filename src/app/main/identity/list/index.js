import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import LeftSidebar from './components/LeftSidebar';
import Table from './components/IdentityTable';
import { IdentityTableProvider } from './components/IdentityTable.context';

function Page() {
  return (
    <IdentityTableProvider>
      <FusePageCarded
        header={<Header />}
        leftSidebarOpen
        leftSidebarContent={<LeftSidebar />}
        content={<Table />}
        scroll="content"
      />
    </IdentityTableProvider>
  );
}

export default Page;
