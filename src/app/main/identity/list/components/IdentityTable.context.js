import { createContext, useState, useContext } from 'react';

export const IdentityTableContext = createContext({
  accountType: '',
  setAccountType: () => {},
  searchValue: '',
  setSearchValue: () => {},
});

export const IdentityTableProvider = (props) => {
  const { children } = props;
  const [accountType, setAccountType] = useState('');
  const [searchValue, setSearchValue] = useState('');

  return (
    <IdentityTableContext.Provider
      value={{
        accountType,
        setAccountType,
        searchValue,
        setSearchValue,
      }}
    >
      {children}
    </IdentityTableContext.Provider>
  );
};

export const IdentityTableConsumer = IdentityTableContext.Consumer;
export const useIdentityTable = () => useContext(IdentityTableContext);
