import SearchIcon from '@mui/icons-material/Search';
import Autocomplete from '@mui/material/Autocomplete';
import Box from '@mui/material/Box';
import InputAdornment from '@mui/material/InputAdornment';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import _ from 'lodash';
import { useState } from 'react';
import { useIdentityTable } from './IdentityTable.context';

const OptionType = {
  field: 'field',
  operator: 'operator',
  conjunction: 'conjunction',
  unknown: 'unknown',
};

const Operators = [':*', ':'];
const Conjunctions = ['and', 'or'];

function SearchBox(props) {
  const { searchValue, setSearchValue } = useIdentityTable();

  const [open, setOpen] = useState(false);
  const fields = [
    'firstName',
    'lastName',
    'courses.year',
    'site',
    'cna',
    'accountType',
    'chineseName',
    'department',
    'courses.progCode',
    'courses.module',
    'courses.acadClass',
  ];

  const [options, setOptions] = useState(
    fields.map((field) => {
      return {
        type: OptionType.field,
        parent: [],
        value: `${field} `,
        label: field,
      };
    })
  );

  function handleInputChange(event, _value, reason) {
    extractValue(_value);
  }

  function extractValue(_value) {
    let searchArr = [];

    const splitKql = _value.split(/(and|or)/);

    const fieldRegexString = fields.join('|');
    const operatorRegexString = Operators.join('|').replaceAll('*', '\\*');
    const conjunctionRegexString = Conjunctions.join('|');
    splitKql.forEach((query) => {
      query = query.trim();
      // match field
      const fieldRegex = new RegExp(`(${fieldRegexString})`);
      const operatorRegex = new RegExp(`(${operatorRegexString})`);
      const conjunctionRegex = new RegExp(`(${conjunctionRegexString})`);
      const phraseRegex = new RegExp(`(${fieldRegexString})( |)(${operatorRegexString})( |)(.*)`);

      const fieldMatch = new RegExp(fieldRegex, 'g').test(query);
      const operatorMatch = new RegExp(operatorRegex, 'g').test(query);
      const conjunctionMatch = new RegExp(conjunctionRegex, 'g').test(query);
      const phraseMatch = new RegExp(phraseRegex, 'g').test(query);

      if (phraseMatch) {
        const result = query.trim().split(phraseRegex);
        searchArr = searchArr.concat([
          {
            type: OptionType.field,
            value: result[1],
          },
          {
            type: OptionType.operator,
            value: result[3],
          },
          {
            type: OptionType.unknown,
            value: result[5],
          },
        ]);
      } else if (fieldMatch) {
        searchArr = searchArr.concat([
          {
            type: OptionType.field,
            value: query,
          },
        ]);
      } else if (operatorMatch) {
        searchArr = searchArr.concat([
          {
            type: OptionType.operator,
            value: query,
          },
        ]);
      } else if (conjunctionMatch) {
        searchArr = searchArr.concat([
          {
            type: OptionType.conjunction,
            value: query,
          },
        ]);
      }
    });

    let newSearchValue = '';
    searchArr.forEach((kql) => {
      if (kql.type === OptionType.field) {
        newSearchValue += `${kql.value} `;
      } else if (kql.type === OptionType.operator) {
        newSearchValue += `${kql.value} `;
      } else if (kql.type === OptionType.conjunction) {
        newSearchValue += `${kql.value} `;
      } else if (kql.type === OptionType.unknown) {
        newSearchValue += `'${kql.value}' `;
      }
    });
    setSearchValue(newSearchValue);

    if (searchArr.length > 0) {
      const last = _.last(searchArr);
      const endWithSpace = _value.endsWith(' ');
      if (endWithSpace && last.type === OptionType.field) {
        changeSelectOptions(_value, [OptionType.operator, OptionType.conjunction]);
      } else if (endWithSpace && last.type === OptionType.unknown && last.value !== '') {
        changeSelectOptions(_value, [OptionType.conjunction]);
      } else if (endWithSpace && last.type === OptionType.conjunction) {
        changeSelectOptions(_value, [OptionType.field]);
      } else if (last.type === OptionType.field) {
        changeSelectOptions(_value, [OptionType.operator]);
      } else if (last.type === OptionType.operator) {
        changeSelectOptions(_value, []);
      } else if (last.type === OptionType.unknown) {
        changeSelectOptions(_value, []);
      }
    } else {
      changeSelectOptions('', [OptionType.field]);
    }
  }

  function changeSelectOptions(perfix, types) {
    const newOptions = [];
    types.forEach((type) => {
      if (type === OptionType.field) {
        fields.forEach((field) => {
          newOptions.push({
            type: OptionType.field,
            parent: [],
            value: `${perfix}${field} `,
            label: field,
          });
        });
      } else if (type === OptionType.operator) {
        Operators.forEach((field) => {
          newOptions.push({
            type: OptionType.operator,
            parent: [],
            value: `${perfix}${field} `,
            label: field,
          });
        });
      } else if (type === OptionType.conjunction) {
        Conjunctions.forEach((field) => {
          newOptions.push({
            type: OptionType.conjunction,
            parent: [],
            value: `${perfix}${field} `,
            label: field,
          });
        });
      }
    });

    setOptions(newOptions);
  }

  return (
    <div className="flex flex-1 flex-col items-center space-x-8 w-full sm:w-auto">
      <Autocomplete
        fullWidth
        open={open}
        onFocus={() => setOpen(true)}
        onBlur={() => setOpen(false)}
        freeSolo
        options={options}
        sx={{
          backgroundColor: '#FFFFFF',
          boxShadow:
            '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
          borderBottomLeftRadius: open && options.length > 0 ? 0 : 16,
          borderBottomRightRadius: open && options.length > 0 ? 0 : 16,
          '& fieldset': {
            border: 'none',
          },
        }}
        PaperComponent={(props) => (
          <Paper
            {...props}
            sx={{
              borderTopLeftRadius: 0,
              borderTopRightRadius: 0,
              '& .MuiAutocomplete-listbox': {
                padding: '0',
              },
            }}
          />
        )}
        renderInput={(params) => (
          <TextField
            placeholder="Search"
            fullWidth
            variant="outlined"
            {...params}
            InputProps={{
              ...params.InputProps,
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
          />
        )}
        getOptionLabel={(option) => {
          if (typeof option === 'string') {
            return option;
          }
          return option.value;
        }}
        renderOption={(p, option) => (
          <Box
            component="li"
            {...p}
            sx={{
              padding: '0 !important',
            }}
          >
            {option.type === OptionType.field && (
              <Box
                sx={{
                  backgroundColor: '#fff9e8',
                  color: '#8a6a0a',
                  padding: 1,
                  paddingTop: '6px',
                  paddingBottom: '6px',
                  height: 33,
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="10"
                  viewBox="0 0 16 10"
                  classNmae="euiIcon euiIcon--medium"
                  focusable="false"
                  role="img"
                  aria-hidden="true"
                >
                  <path d="M8 9a5 5 0 110-8 5 5 0 110 8zm.75-.692a4 4 0 100-6.615A4.981 4.981 0 0110 5a4.981 4.981 0 01-1.25 3.308zM4.133 8V5.559h2.496v-.625H4.133V2.996h2.719v-.633H3.43V8h.703z" />
                </svg>
              </Box>
            )}

            {option.type === OptionType.operator && (
              <Box
                sx={{
                  backgroundColor: '#e6f1fa',
                  color: '#006bb8',
                  padding: 1,
                  paddingTop: '6px',
                  paddingBottom: '6px',
                  height: 33,
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  className="euiIcon euiIcon--medium"
                  focusable="false"
                  role="img"
                  aria-hidden="true"
                >
                  <path d="M11.192 10.145l2.298-1.792c.259-.196.259-.509 0-.706l-2.298-1.792c-.256-.196-.256-.513 0-.708a.81.81 0 01.93 0l2.3 1.791c.772.59.77 1.537 0 2.124l-2.3 1.791a.81.81 0 01-.93 0c-.256-.195-.256-.512 0-.708zm-6.384-4.29L2.51 7.647c-.259.196-.259.509 0 .706l2.298 1.792c.256.196.256.513 0 .708a.81.81 0 01-.93 0l-2.3-1.791c-.772-.59-.77-1.537 0-2.124l2.3-1.791a.81.81 0 01.93 0c.256.195.256.512 0 .708zM6.5 6h3a.5.5 0 010 1h-3a.5.5 0 010-1zm0 3h3a.5.5 0 010 1h-3a.5.5 0 010-1z" />
                </svg>
              </Box>
            )}

            {option.type === OptionType.conjunction && (
              <Box
                sx={{
                  backgroundColor: '#f4f1f8',
                  color: '#7c609e',
                  padding: 1,
                  paddingTop: '6px',
                  paddingBottom: '6px',
                  height: 33,
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  viewBox="0 0 16 16"
                  className="euiIcon euiIcon--medium"
                  focusable="false"
                  role="img"
                  aria-hidden="true"
                >
                  <path d="M5 12a4 4 0 100-8 4 4 0 000 8zm0 1A5 5 0 115 3a5 5 0 010 10zm6-1a4 4 0 100-8 4 4 0 000 8zm0 1a5 5 0 110-10 5 5 0 010 10z" />
                </svg>
              </Box>
            )}
            <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...p}>
              {option.label}
            </Box>
          </Box>
        )}
        onInputChange={handleInputChange}
      />
    </div>
  );
}

export default SearchBox;
