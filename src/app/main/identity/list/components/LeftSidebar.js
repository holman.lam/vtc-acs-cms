import { styled } from '@mui/material/styles';
import MuiCard from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { useDispatch } from 'react-redux';
import Button from '@mui/material/Button';
import { openDialog } from 'app/store/fuse/dialogSlice';
import { Divider, Hidden, Typography, Box, TextField } from '@mui/material';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import DownloadIcon from '@mui/icons-material/Download';
import SearchBox from './SearchBox';
import IdentityForm from './IdentityForm';
import { useIdentityTable } from './IdentityTable.context';

const Card = styled(MuiCard)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    boxShadow: 'none',
    background: 'none',
  },
}));

function LeftSidebar(props) {
  const dispatch = useDispatch();
  const { accountType, setAccountType } = useIdentityTable();

  const handleAddIdentity = () => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <IdentityForm />,
      })
    );
  };

  return (
    <div>
      <Card className="m-12">
        <CardContent className="flex flex-col items-center">
          <Button
            variant="contained"
            color="primary"
            fullWidth
            className="mb-12"
            onClick={handleAddIdentity}
          >
            Add Identity
          </Button>

          <Button variant="contained" color="primary" fullWidth className="mb-12">
            Import
          </Button>

          <Button variant="text" color="primary" fullWidth className="mb-12">
            <DownloadIcon />
            <Typography variant="body2">Download Input Template</Typography>
          </Button>

          <Divider flexItem className="mb-12" />

          <ListItem disablePadding>
            <ListItemButton selected={accountType === ''} onClick={() => setAccountType('')}>
              <img
                className="w-20 h-20 mr-12"
                src="assets/images/icons/all_contacts.svg"
                alt="All Contacts"
              />
              <ListItemText primary="All contacts" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton
              selected={accountType === 'teacher'}
              onClick={() => setAccountType('teacher')}
            >
              <img
                className="w-20 h-20 mr-12"
                src="assets/images/icons/teachers.svg"
                alt="All Contacts"
              />
              <ListItemText primary="Teachers" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton
              selected={accountType === 'student'}
              onClick={() => setAccountType('student')}
            >
              <img
                className="w-20 h-20 mr-12"
                src="assets/images/icons/students.svg"
                alt="All Contacts"
              />
              <ListItemText primary="Students" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton
              selected={accountType === 'local'}
              onClick={() => setAccountType('local')}
            >
              <img
                className="w-20 h-20 mr-12"
                src="assets/images/icons/local.svg"
                alt="All Contacts"
              />
              <ListItemText primary="Local" />
            </ListItemButton>
          </ListItem>

          <Divider flexItem className="mt-12" />

          <Box className="w-full">
            <Typography className="my-12 ">Filter</Typography>

            <TextField size="small" placeholder="Value" className="mb-12" />

            <Button variant="contained" color="primary" fullWidth>
              Filter
            </Button>
          </Box>

          <Hidden lgUp>
            <SearchBox />
          </Hidden>
        </CardContent>
      </Card>
    </div>
  );
}

export default LeftSidebar;
