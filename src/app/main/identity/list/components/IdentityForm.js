import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import FormHelperText from '@mui/material/FormHelperText';
import Toolbar from '@mui/material/Toolbar';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { createIdentity, getIdentity, updateIdentity } from '@api';
import { Emitter, Event } from '@emitter';
import { useEffect, useState } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import { closeDialog } from 'app/store/fuse/dialogSlice';
import { combineDateAndTimeToDateTime, convertApiDateTimeToDate } from 'app/utils/date';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import useDepartments from 'app/hooks/useDepartments';

function IdentityForm({ id }) {
  const dispatch = useDispatch();
  const [{ data: departments }] = useDepartments();
  const [loading, setLoading] = useState(true);

  const formik = useFormik({
    initialValues: {
      id: undefined,
      cna: '',
      smartCardId: '',
      firstName: '',
      lastName: '',
      chineseName: '',
      department: '',
      accountType: 'Local',
      entryDate: null,
      entryTime: null,
      expiryDate: null,
      expiryTime: null,
    },
    validateOnMount: true,
    validationSchema: Yup.object({
      cna: Yup.string().required().label('CNA'),
      smartCardId: Yup.string().required().label('Smart Card ID'),
      firstName: Yup.string().required().label('First Name'),
      lastName: Yup.string().required().label('Last Name'),
      chineseName: Yup.string().required().label('Chinese Name'),
      department: Yup.string().required().label('Department'),
      entryDate: Yup.date().nullable().required().label('Entry Date'),
      entryTime: Yup.date().nullable().required().label('Entry Time'),
      expiryDate: Yup.date().nullable().required().label('Expiry Date'),
      expiryTime: Yup.date().nullable().required().label('Expiry Time'),
    }),
    onSubmit: async (values, helpers) => {
      const { entryDate, entryTime, expiryDate, expiryTime, ...rest } = values;
      const data = {
        ...rest,
      };
      data.dateOfEntry = combineDateAndTimeToDateTime(entryDate, entryTime);
      data.expiryDate = combineDateAndTimeToDateTime(expiryDate, expiryTime);
      if (id) {
        // waiting cms user edit api
        await handleUpdateRecord(data, helpers);
      } else {
        await handleCreateRecord(data, helpers);
      }
    },
  });

  async function handleCreateRecord(data, helpers) {
    const res = await createIdentity({ data });
    if (res.ok) {
      Emitter.emit(Event.CMS_USER_CREATED, res.data);
      dispatch(closeDialog());
    }
  }

  async function handleUpdateRecord(data, helpers) {
    const res = await updateIdentity({ data });
    if (res.ok) {
      Emitter.emit(Event.CMS_USER_UPDATED, res.data);
      dispatch(closeDialog());
    }
  }

  useEffect(() => {
    if (id) {
      getIdentity({ id }).then((response) => {
        const { data } = response;
        formik.setValues({
          id: data.id || '',
          cna: data.cna || '',
          smartCardId: data.smartCardId || '',
          firstName: data.firstName || '',
          lastName: data.lastName || '',
          chineseName: data.chineseName || '',
          department: data.department || '',
          entryDate: convertApiDateTimeToDate(data.dateOfEntry),
          entryTime: convertApiDateTimeToDate(data.dateOfEntry),
          expiryDate: convertApiDateTimeToDate(data.expiryDate),
          expiryTime: convertApiDateTimeToDate(data.expiryDate),
        });
        setLoading(false);
      });
    } else {
      setLoading(false);
    }
  }, [id]);

  return (
    <>
      <AppBar position="static" color="secondary" elevation={0}>
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            {id ? 'Edit' : 'New'} Identity
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        {loading ? (
          <div className="flex items-center justify-center h-full">
            <FuseLoading />
          </div>
        ) : (
          <div className="my-24">
            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/name.svg"
                alt="smartCardId"
              />

              <TextField
                autoFocus
                variant="outlined"
                fullWidth
                label="CNA"
                name="cna"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.cna}
                helperText={Boolean(formik.touched.cna && formik.errors.cna) && formik.errors.cna}
                error={Boolean(formik.touched.cna && formik.errors.cna)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/name.svg"
                alt="smartCardId"
              />

              <TextField
                variant="outlined"
                fullWidth
                label="Smart Card ID"
                name="smartCardId"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.smartCardId}
                helperText={
                  Boolean(formik.touched.smartCardId && formik.errors.smartCardId) &&
                  formik.errors.smartCardId
                }
                error={Boolean(formik.touched.smartCardId && formik.errors.smartCardId)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/username.svg"
                alt="smartCardId"
              />

              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    label="First Name"
                    name="firstName"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.firstName}
                    helperText={
                      Boolean(formik.touched.firstName && formik.errors.firstName) &&
                      formik.errors.firstName
                    }
                    error={Boolean(formik.touched.firstName && formik.errors.firstName)}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    label="Last Name"
                    name="lastName"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.lastName}
                    helperText={
                      Boolean(formik.touched.lastName && formik.errors.lastName) &&
                      formik.errors.lastName
                    }
                    error={Boolean(formik.touched.lastName && formik.errors.lastName)}
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    fullWidth
                    label="Chinese Name"
                    name="chineseName"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    value={formik.values.chineseName}
                    helperText={
                      Boolean(formik.touched.chineseName && formik.errors.chineseName) &&
                      formik.errors.chineseName
                    }
                    error={Boolean(formik.touched.chineseName && formik.errors.chineseName)}
                  />
                </Grid>
              </Grid>
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/type.svg"
                alt="Department"
              />
              <FormControl
                fullWidth
                error={Boolean(formik.touched.department && formik.errors.department)}
              >
                <InputLabel id="accountType">Department</InputLabel>
                <Select
                  id="department"
                  label="Department"
                  name="department"
                  value={formik.values.department}
                  onChange={formik.handleChange}
                >
                  {departments.map((d) => (
                    <MenuItem value={d.name}>{d.name}</MenuItem>
                  ))}
                </Select>
                <FormHelperText>
                  {Boolean(formik.touched.department && formik.errors.department) &&
                    formik.errors.department}
                </FormHelperText>
              </FormControl>
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/calendar.svg"
                alt="location"
              />

              <Grid container spacing={1} columns={10}>
                <Grid item xs={6}>
                  <DatePicker
                    label="Date of entry"
                    value={formik.values.entryDate}
                    onChange={(newValue) => {
                      formik.setFieldValue('entryDate', newValue);
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={Boolean(formik.touched.entryDate && formik.errors.entryDate)}
                        helperText={
                          Boolean(formik.touched.entryDate && formik.errors.entryDate) &&
                          formik.errors.entryDate
                        }
                        sx={{ flex: 2 / 3 }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <TimePicker
                    ampm={false}
                    label="Entry Time"
                    value={formik.values.entryTime}
                    onChange={(newValue) => {
                      formik.setFieldValue('entryTime', newValue);
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={Boolean(formik.touched.entryTime && formik.errors.entryTime)}
                        helperText={
                          Boolean(formik.touched.entryTime && formik.errors.entryTime) &&
                          formik.errors.entryTime
                        }
                        sx={{ flex: 1 / 3 }}
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>

            <div className="flex flex-row mb-24">
              <img
                className="w-20 h-20 mr-12 my-16"
                src="assets/images/icons/calendar.svg"
                alt="location"
              />

              <Grid container spacing={1} columns={10}>
                <Grid item xs={6}>
                  <DatePicker
                    label="Expiry date"
                    value={formik.values.expiryDate}
                    onChange={(newValue) => {
                      formik.setFieldValue('expiryDate', newValue);
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={Boolean(formik.touched.expiryDate && formik.errors.expiryDate)}
                        helperText={
                          Boolean(formik.touched.expiryDate && formik.errors.expiryDate) &&
                          formik.errors.expiryDate
                        }
                        sx={{ flex: 2 / 3 }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <TimePicker
                    ampm={false}
                    label="Expiry Time"
                    value={formik.values.expiryTime}
                    onChange={(newValue) => {
                      formik.setFieldValue('expiryTime', newValue);
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={Boolean(formik.touched.expiryTime && formik.errors.expiryTime)}
                        helperText={
                          Boolean(formik.touched.expiryTime && formik.errors.expiryTime) &&
                          formik.errors.expiryDate
                        }
                        sx={{ flex: 1 / 3 }}
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </div>
        )}
      </DialogContent>
      <DialogActions className="px-24 pb-24">
        <Button disabled={formik.isSubmitting} onClick={() => formik.handleSubmit()}>
          {id ? 'Save' : 'Add'}
        </Button>
      </DialogActions>
    </>
  );
}

export default IdentityForm;
