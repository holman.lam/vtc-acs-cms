import { useEffect, useRef, useState } from 'react';
import { getIdentities } from '@api';
import Table from 'app/shared-components/Table';
import Box from '@mui/material/Box';
import { Emitter, Event } from '@emitter';
import { useDebounce } from 'app/hooks';
import EditIcon from '@mui/icons-material/Edit';
import { useDispatch } from 'react-redux';
import { openDialog } from 'app/store/fuse/dialogSlice';
import { convertApiDateTimeToDisplayDateTime } from 'app/utils/date';
import { Typography, Button } from '@mui/material';
import { useIdentityTable } from './IdentityTable.context';
import IdentityForm from './IdentityForm';
import IdentityEditForm from './IdentityEditForm';
import AssignForm from './AssignForm';

const columns = [
  { id: 'cna', label: 'CNA', minWidth: 170 },
  { id: 'smartCardId', label: 'Smart Card ID.', minWidth: 170 },
  { id: 'firstName', label: 'First Name', minWidth: 150 },
  { id: 'lastName', label: 'Last Name', minWidth: 150 },
  { id: 'chineseName', label: 'Chinese Name', minWidth: 170 },
  { id: 'department', label: 'Department', minWidth: 100 },
  { id: 'accountType', label: 'Account Type', minWidth: 100 },
  { id: 'courseNo', label: 'Course No.', minWidth: 100 },
  { id: 'module', label: 'Module', minWidth: 100 },
  { id: 'year', label: 'Year', minWidth: 100 },
  { id: 'class', label: 'Class', minWidth: 100 },
  { id: 'site', label: 'Site', minWidth: 100 },
  {
    id: 'dateOfEntry',
    label: 'Date of entry',
    minWidth: 150,
    render: ({ rowData }) => convertApiDateTimeToDisplayDateTime(rowData.dateOfEntry),
  },
  {
    id: 'expiryDate',
    label: 'Expiry date',
    minWidth: 150,
    render: ({ rowData }) => convertApiDateTimeToDisplayDateTime(rowData.expiryDate),
  },
  { id: 'buildingAccess', label: 'Building Access', minWidth: 170 },
  { id: 'cwa', label: 'CWA', minWidth: 100 },
  { id: 'cwb', label: 'CWB', minWidth: 100 },
  { id: 'cwc', label: 'CWC', minWidth: 100 },
  { id: 'cwe', label: 'CWE', minWidth: 100 },
];

function IdentityTable(props) {
  const dispatch = useDispatch();
  const tableRef = useRef(null);
  const { searchValue } = useIdentityTable();
  const debouncedsearchValue = useDebounce(searchValue, 500);
  const [selected, setSelected] = useState([]);

  function reloadTable() {
    tableRef.current.reload();
  }

  useEffect(() => {
    reloadTable();
  }, [debouncedsearchValue]);

  useEffect(() => {
    Emitter.on(Event.CMS_USER_CREATED, reloadTable);
    Emitter.on(Event.CMS_USER_UPDATED, reloadTable);
    return () => {
      Emitter.off(Event.CMS_USER_CREATED, reloadTable);
      Emitter.off(Event.CMS_USER_UPDATED, reloadTable);
    };
  }, []);

  const dataSource = ({ page, pageSize, sort }) => {
    const data = {
      currentPage: page,
      pageSize,
      filterQuery: searchValue,
      order: [
        {
          column: 'cna',
          dir: sort.direction,
        },
      ],
      // columns: [
      //   // column 0 is used for sorting
      //   {
      //     data: sort.id,
      //     searchable: true,
      //     orderable: true,
      //     search: {
      //       value: '',
      //       regex: false,
      //     },
      //   },
      //   // for searching
      //   {
      //     data: 'accountType',
      //     searchable: true,
      //     orderable: true,
      //     search: {
      //       value: `ONE("${debouncedsearchValue}")`,
      //       regex: false,
      //     },
      //   },
      // ],
    };
    return new Promise((resolve, reject) => {
      getIdentities({
        data,
      }).then((response) => {
        if (response.ok) {
          return resolve({
            data: response.data.content,
            count: response.data.total,
          });
        }
        return reject(response?.data?.errorMessage || response.problem);
      });
    });
  };

  const handleEditIdentity = (id) => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <IdentityEditForm id={id} />,
      })
    );
  };

  const handleAssignIdentity = () => {
    dispatch(
      openDialog({
        maxWidth: 'xs',
        fullWidth: true,
        children: <AssignForm selectedIds={selected} />,
      })
    );
  };

  const handleSelect = (row) => {
    const selectedIndex = selected.indexOf(row.cna);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, row.cna);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleSelectAllClick = (event, rows) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.cna);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  function ActionBar() {
    return (
      <Box
        container
        className="flex items-center justify-center"
        sx={{
          backgroundColor: (theme) => theme.palette.grey[300],
          borderRadius: (theme) => theme.spacing(1),
          p: (theme) => theme.spacing(1),
        }}
      >
        <Typography
          fontWeight={600}
          color="black"
          sx={{ mr: 2 }}
        >{`${selected.length} results has been selected.`}</Typography>

        <Button variant="text" size="small" onClick={handleAssignIdentity}>
          Assign
        </Button>
      </Box>
    );
  }

  return (
    <Table
      ref={tableRef}
      columns={columns}
      dataSource={dataSource}
      defaultSort={{ direction: 'asc', id: 'cna' }}
      actions={[
        (rowData) => ({
          icon: <EditIcon />,
          tooltip: 'Edit',
          onClick: () => handleEditIdentity(rowData.cna),
        }),
      ]}
      selection
      selected={selected}
      isSelected={(row) => selected.indexOf(row.cna) >= 0}
      onSelect={handleSelect}
      onSelectAll={handleSelectAllClick}
      actionBar={selected.length > 0 ? <ActionBar /> : undefined}
    />
  );
}

export default IdentityTable;
