import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import FormHelperText from '@mui/material/FormHelperText';
import Toolbar from '@mui/material/Toolbar';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { createIdentity, getIdentity, updateIdentity } from '@api';
import { Emitter, Event } from '@emitter';
import { useEffect, useState, useRef, createRef } from 'react';
import FuseLoading from '@fuse/core/FuseLoading';
import { closeDialog } from 'app/store/fuse/dialogSlice';
import { combineDateAndTimeToDateTime, convertApiDateTimeToDate } from 'app/utils/date';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import useDepartments from 'app/hooks/useDepartments';
import CustomTextField from './form-components/CustomTextField'
import CustomNameTextField from './form-components/CustomNameTextField'
import CustomSelectField from './form-components/CustomSelectField'
import CustomDateTimeField from './form-components/CustomDateTimeField'

function IdentityEditForm({ id }) {
  const dispatch = useDispatch();
  const [{ data: departments }] = useDepartments();
  const [loading, setLoading] = useState(false);
  const [isEditable, setIsEditable] = useState(false);
  const fieldRef = useRef([...Array(7)].map((_, i) => createRef()));
  let index = 0;

  const formik = useFormik({
    initialValues: {
      id: undefined,
      cna: '',
      smartCardId: '',
      firstName: '',
      lastName: '',
      chineseName: '',
      department: '',
      entryDate: null,
      entryTime: null,
      expiryDate: null,
      expiryTime: null,
    },
    validateOnMount: true,
    validationSchema: Yup.object({
      cna: Yup.string().required().label('CNA'),
      smartCardId: Yup.string().required().label('Smart Card ID'),
      firstName: Yup.string().required().label('First Name'),
      lastName: Yup.string().required().label('Last Name'),
      chineseName: Yup.string().required().label('Chinese Name'),
      department: Yup.string().required().label('Department'),
      entryDate: Yup.date().nullable().required().label('Entry Date'),
      entryTime: Yup.date().nullable().required().label('Entry Time'),
      expiryDate: Yup.date().nullable().required().label('Expiry Date'),
      expiryTime: Yup.date().nullable().required().label('Expiry Time'),
    }),
    onSubmit: async (values, helpers) => {
      const { entryDate, entryTime, expiryDate, expiryTime, ...rest } = values;
      const data = {
        ...rest,
      };
      data.dateOfEntry = combineDateAndTimeToDateTime(entryDate, entryTime);
      data.expiryDate = combineDateAndTimeToDateTime(expiryDate, expiryDate);
      if (id) {
        await handleUpdateRecord(data, helpers);
      } else { // No Create record on this page
        // await handleCreateRecord(data, helpers);
      }
    },
  });

  async function handleCreateRecord(data, helpers) {
    // const res = await createIdentity({ data });
    // if (res.ok) {
    //   Emitter.emit(Event.CMS_USER_CREATED, res.data);
    //   dispatch(closeDialog());
    // }
  }

  async function handleUpdateRecord(data, helpers) {
    const res = await updateIdentity({ data });
    if (res.ok) {
      Emitter.emit(Event.CMS_USER_UPDATED, res.data);
      // dispatch(closeDialog());
      fieldRef.current.map(ref => ref.current.closeEdit());
    }
  }

  useEffect(() => {
    if (id) {
      getIdentity({ id }).then((response) => {
        const { data } = response;
        formik.setValues({
          id: data.id || '',
          cna: data.cna || '',
          smartCardId: data.smartCardId || '',
          firstName: data.firstName || '',
          lastName: data.lastName || '',
          chineseName: data.chineseName || '',
          department: data.department || '',
          accountType: data.accountType || '',
          site: data.site || '',
          entryDate: convertApiDateTimeToDate(data.dateOfEntry),
          entryTime: convertApiDateTimeToDate(data.dateOfEntry),
          expiryDate: convertApiDateTimeToDate(data.expiryDate),
          expiryTime: convertApiDateTimeToDate(data.expiryDate),
        });
        if(data.accountType == "Local"){
          setIsEditable(true);
        }
        
        setLoading(false);
      });
        setLoading(false);
    } else {
      setLoading(false);
    }
  }, [id]);

  return (
    <>
      <AppBar position="static" color="secondary" elevation={0}>
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            {id ? 'Edit' : 'New'} Identity
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        {loading ? (
          <div className="flex items-center justify-center h-full">
            <FuseLoading />
          </div>
        ) : (
          <div className="my-24">
            <div className="flex flex-row mb-24">
            <CustomTextField
              isEditable={false}
              image="assets/images/icons/name.svg"
              name="cna"
              label="CNA"
              value={formik.values.cna}
              onSubmit={formik.handleSubmit}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              helperText={Boolean(formik.touched.cna && formik.errors.cna) && formik.errors.cna}
              error={Boolean(formik.touched.cna && formik.errors.cna)}
            />
            </div>

            <div className="flex flex-row mb-24">
              <CustomTextField
                ref={fieldRef.current[index++]}
                isEditable={isEditable}
                image="assets/images/icons/name.svg"
                name="smartCardId"
                label="Smart Card ID"
                value={formik.values.smartCardId}
                onSubmit={formik.handleSubmit}
                isSubmitting={formik.isSubmitting}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                helperText={Boolean(formik.touched.smartCardId && formik.errors.smartCardId) && formik.errors.smartCardId}
                error={Boolean(formik.touched.smartCardId && formik.errors.smartCardId)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <CustomNameTextField
                  ref={fieldRef.current[index++]}
                  isEditable={isEditable}
                  image="assets/images/icons/name.svg"
                  onSubmit={formik.handleSubmit}
                  isSubmitting={formik.isSubmitting}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}

                  firstNameValue={formik.values.firstName}
                  firstNameHelperText={Boolean(formik.touched.firstName && formik.errors.firstName) && formik.errors.firstName}
                  firstNameError={Boolean(formik.touched.firstName && formik.errors.firstName)}

                  lastNameValue={formik.values.lastName}
                  lastNameHelperText={Boolean(formik.touched.lastName && formik.errors.lastName) && formik.errors.lastName}
                  lastNameError={Boolean(formik.touched.lastName && formik.errors.lastName)}
                  
                  chineseNameValue={formik.values.chineseName}
                  chineseNameHelperText={Boolean(formik.touched.chineseName && formik.errors.chineseName) && formik.errors.chineseName}
                  chineseNameError={Boolean(formik.touched.chineseName && formik.errors.chineseName)}
                />
            </div>

            <div className="flex flex-row mb-24">
              <CustomSelectField
                ref={fieldRef.current[index++]}
                isEditable={isEditable}
                image="assets/images/icons/type.svg"
                name="department"
                label="Department"
                data={departments}
                value={formik.values.department}
                onSubmit={formik.handleSubmit}
                isSubmitting={formik.isSubmitting}
                onBlur={formik.handleBlur}
                onChange={formik.department}
                helperText={Boolean(formik.touched.department && formik.errors.department) && formik.errors.department}
                error={Boolean(formik.touched.department && formik.errors.department)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <CustomSelectField
                ref={fieldRef.current[index++]}
                isEditable={false}
                image="assets/images/icons/type.svg"
                name="accountType"
                label="Account Type"
                value={formik.values.accountType}
                data={[]}
                onSubmit={formik.handleSubmit}
                isSubmitting={formik.isSubmitting}
                onBlur={formik.handleBlur}
                onChange={formik.accountType}
                helperText={Boolean(formik.touched.accountType && formik.errors.accountType) && formik.errors.accountType}
                error={Boolean(formik.touched.accountType && formik.errors.accountType)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <CustomSelectField
                ref={fieldRef.current[index++]}
                isEditable={false}
                image="assets/images/icons/type.svg"
                name="site"
                label="Site"
                value={formik.values.site}
                data={[]}
                onSubmit={formik.handleSubmit}
                isSubmitting={formik.isSubmitting}
                onBlur={formik.handleBlur}
                onChange={formik.site}
                helperText={Boolean(formik.touched.site && formik.errors.site) && formik.errors.site}
                error={Boolean(formik.touched.site && formik.errors.site)}
              />
            </div>

            <div className="flex flex-row mb-24">
              <CustomDateTimeField
                ref={fieldRef.current[index++]}
                isEditable={isEditable}
                image="assets/images/icons/calendar.svg"
                dateLabel="Date of entry"
                dateName="entryDate"
                dateValue={formik.values.entryDate}
                dateHelperText={
                  Boolean(formik.touched.entryDate && formik.errors.entryDate) &&
                  formik.errors.entryDate
                }
                dateError={Boolean(formik.touched.entryDate && formik.errors.entryDate)}
                dateOnChange={(newValue) => {
                  formik.setFieldValue('entryDate', newValue);
                }}

                timeLabel="Entry Time"
                timeName="entryTime"
                timeValue={formik.values.entryTime}
                timeHelperText={
                  Boolean(formik.touched.entryTime && formik.errors.entryTime) &&
                  formik.errors.entryTime
                }
                timeError={Boolean(formik.touched.entryTime && formik.errors.entryTime)}
                timeOnChange={(newValue) => {
                  formik.setFieldValue('entryTime', newValue);
                }}

                onSubmit={formik.handleSubmit}
                isSubmitting={formik.isSubmitting}
                onBlur={formik.handleBlur}
              />
            </div>

            <div className="flex flex-row mb-24">
            <CustomDateTimeField
                ref={fieldRef.current[index++]}
                isEditable={isEditable}
                image="assets/images/icons/calendar.svg"
                dateLabel="Expiry date"
                dateName="expiryDate"
                dateValue={formik.values.expiryDate}
                dateHelperText={
                  Boolean(formik.touched.expiryDate && formik.errors.expiryDate) &&
                  formik.errors.expiryDate
                }
                dateError={Boolean(formik.touched.expiryDate && formik.errors.expiryDate)}
                dateOnChange={(newValue) => {
                  formik.setFieldValue('expiryDate', newValue);
                }}

                timeLabel="Expiry Time"
                timeName="expiryTime"
                timeValue={formik.values.expiryTime}
                timeHelperText={
                  Boolean(formik.touched.expiryTime && formik.errors.expiryTime) &&
                  formik.errors.expiryTime
                }
                timeError={Boolean(formik.touched.expiryTime && formik.errors.expiryTime)}
                timeOnChange={(newValue) => {
                  formik.setFieldValue('expiryTime', newValue);
                }}

                onSubmit={formik.handleSubmit}
                isSubmitting={formik.isSubmitting}
                onBlur={formik.handleBlur}
              />
            </div>

            {/* TODO: Add Assign Devices */}
          </div>
        )}
      </DialogContent>
      {/* <DialogActions className="px-24 pb-24">
        <Button disabled={formik.isSubmitting} onClick={() => formik.handleSubmit()}>
          {id ? 'Save' : 'Add'}
        </Button>
      </DialogActions> */}
    </>
  );
}

export default IdentityEditForm;
