
import { forwardRef, useEffect, useState, useImperativeHandle, useCallback } from 'react';
import Typography from '@mui/material/Typography';
import FormHelperText from '@mui/material/FormHelperText';
import Grid from '@mui/material/Grid';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SaveButton from './SaveButton'
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import _ from 'lodash';

const CustomSelectField = forwardRef(
  (
    {
      isEditable,
      image,
      name,
      label,
      value,
      data,

      helperText,
      error,
      isSubmitting,
      onSubmit,
      onBlur,
      onChange,
    },
    ref
  ) => {
    const [editing, setEditing] = useState(false);
    
    useEffect(() => {
      
    }, []);
    
    useImperativeHandle(ref, () => ({
      closeEdit() {
        setEditing(false);
      }
    }));

    const handleEdit = () => {
      setEditing(true);
    }

    return (
      <>
      <Grid container spacing={1} columns={12}>
        <Grid item xs>
          <img
            className="w-20 h-20 mr-12 my-16"
            src={image}
            alt={name}
          />
        </Grid>
        {/* non editing ui*/}
        <Grid item xs={isEditable? 10: 11} display={editing? 'none': 'flex'}>
          <Grid
            container
            direction="column"
            justifyContent="center"
          >
            <Typography variant='body' color='#737373' fontSize="12px">{label}</Typography>
            <Typography variant='body'>{value}</Typography>
          </Grid>
        </Grid>
        {/* editing ui*/}
        <Grid item xs={11} display={editing? 'flex': 'none'}>
          <FormControl
            fullWidth
            error={error}
          >
            <InputLabel id={name}>{label}</InputLabel>
            <Select
              id={name}
              label={label}
              name={name}
              value={value}
              onChange={onChange}
            >
              {data.map((d) => (
                <MenuItem value={d.name}>{d.name}</MenuItem>
              ))}
            </Select>
            <FormHelperText>
              {helperText}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={1} display={!editing && isEditable? 'flex': 'none'}>
          <Tooltip title="Edit">
            <IconButton
              key={`action-${name}`}
              size="small"
              onClick={handleEdit}
            >
              <EditIcon/>
            </IconButton>
          </Tooltip>
        </Grid>

        {/* new line for submiting*/}
        <Grid item xs={1} display={editing? 'flex': 'none'}>
        </Grid>
        <Grid item xs={11} display={editing? 'flex': 'none'}>
          <SaveButton onSubmit={onSubmit} isSubmitting={isSubmitting}/>
        </Grid>
      </Grid>
      </>
    );
  }
);

export default CustomSelectField;
