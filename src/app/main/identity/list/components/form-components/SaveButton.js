
import { forwardRef, useEffect, useState, useImperativeHandle, useCallback } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import _ from 'lodash';

const SaveButton = forwardRef(
  (
    {
      onSubmit,
      isSubmitting,
    },
    ref
  ) => {
    const handleSubmit = () =>{
      onSubmit();
    }
    return (
      <Button 
        variant="contained" 
        onClick={handleSubmit}
        disabled={isSubmitting}
      >Save</Button>
    )
  }
);

export default SaveButton;
