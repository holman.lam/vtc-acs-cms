
import { forwardRef, useEffect, useState, useImperativeHandle, useCallback } from 'react';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SaveButton from './SaveButton'
import _ from 'lodash';

const CustomNameTextField = forwardRef(
  (
    {
      isEditable,
      image,

      firstNameValue,
      firstNameHelperText,
      firstNameError,

      lastNameValue,
      lastNameHelperText,
      lastNameError,

      chineseNameValue,
      chineseNameHelperText,
      chineseNameError,

      isSubmitting,
      onSubmit,
      onBlur,
      onChange,
    },
    ref
  ) => {
    const [editing, setEditing] = useState(false);
    
    useEffect(() => {
      
    }, []);
    
    useImperativeHandle(ref, () => ({
      closeEdit() {
        setEditing(false);
      }
    }));

    const handleEdit = () => {
      setEditing(true);
    }

    return (
      <>
      <Grid container spacing={1} columns={12}>
        <Grid item xs={1}>
          <img
            className="w-20 h-20 mr-12 my-16"
            src={image}
            alt="name"
          />
        </Grid>
        {/* non editing ui*/}
        <Grid item xs={5} display={editing? 'none': 'flex'}>
          <Grid
            container
            direction="column"
            justifyContent="center"
          >
            <Typography variant='body' color='#737373' fontSize="12px">{"First Name"}</Typography>
            <Typography variant='body'>{firstNameValue}</Typography>
          </Grid>
        </Grid>
        {/* editing ui*/}
        <Grid item xs={5} display={editing? 'flex': 'none'}>
          <TextField
            variant="outlined"
            fullWidth
            label="First Name"
            name="firstName"
            onBlur={onBlur}
            onChange={onChange}
            value={firstNameValue}
            helperText={firstNameHelperText}
            error={firstNameError}
            // InputProps={{endAdornment: (<SaveButton onSubmit={onSubmit} isSubmitting={isSubmitting}/>)}}
          />
        </Grid>

        {/* non editing ui*/}
        <Grid item xs={5} display={editing? 'none': 'flex'}>
          <Grid
            container
            direction="column"
            justifyContent="center"
          >
            <Typography variant='body' color='#737373' fontSize="12px">{"Last Name"}</Typography>
            <Typography variant='body'>{lastNameValue}</Typography>
          </Grid>
        </Grid>
        {/* editing ui*/}
        <Grid item xs={6} display={editing? 'flex': 'none'}>
          <TextField
            variant="outlined"
            fullWidth
            label="Last Name"
            name="lastName"
            onBlur={onBlur}
            onChange={onChange}
            value={lastNameValue}
            helperText={lastNameHelperText}
            error={lastNameError}
            // InputProps={{endAdornment: (<SaveButton onSubmit={onSubmit} isSubmitting={isSubmitting}/>)}}
          />
        </Grid>
        <Grid item xs={1} display={!editing && isEditable? 'flex': 'none'}>
          <Tooltip title="Edit">
            <IconButton
              key={`action-name`}
              size="small"
              onClick={handleEdit}
            >
              <EditIcon/>
            </IconButton>
          </Tooltip>
        </Grid>
        <Grid item xs={1} display={isEditable? 'none': 'flex'}></Grid>
        {/* new line for chineseName*/}
        <Grid item xs={1}>
          
        </Grid>
        {/* non editing ui*/}
        <Grid item xs={10} display={editing? 'none': 'flex'}>
          <Grid
            container
            direction="column"
            justifyContent="center"
          >
            <Typography variant='body' color='#737373' fontSize="12px">{"Chinese Name"}</Typography>
            <Typography variant='body'>{chineseNameValue}</Typography>
          </Grid>
        </Grid>
        {/* editing ui*/}
        <Grid item xs={11} display={editing? 'flex': 'none'}>
          <TextField
            variant="outlined"
            fullWidth
            label="Chinese Name"
            name="chineseName"
            onBlur={onBlur}
            onChange={onChange}
            value={chineseNameValue}
            helperText={chineseNameHelperText}
            error={chineseNameError}
            // InputProps={{endAdornment: (<SaveButton onSubmit={onSubmit} isSubmitting={isSubmitting}/>)}}
          />
        </Grid>

        {/* new line for submiting*/}
        <Grid item xs={1} display={editing? 'flex': 'none'}>
        </Grid>
        <Grid item xs={11} display={editing? 'flex': 'none'}>
          <SaveButton onSubmit={onSubmit} isSubmitting={isSubmitting}/>
        </Grid>
      </Grid>
      </>
    );
  }
);

export default CustomNameTextField;
