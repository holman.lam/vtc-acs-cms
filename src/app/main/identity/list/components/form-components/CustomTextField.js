
import { forwardRef, useEffect, useState, useImperativeHandle, useCallback } from 'react';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SaveButton from './SaveButton'
import _ from 'lodash';

const CustomTextField = forwardRef(
  (
    {
      isEditable,
      image,
      name,
      label,
      value,

      helperText,
      error,
      isSubmitting,
      onSubmit,
      onBlur,
      onChange,
    },
    ref
  ) => {
    const [editing, setEditing] = useState(false);
    
    useEffect(() => {
      
    }, []);
    
    useImperativeHandle(ref, () => ({
      closeEdit() {
        setEditing(false);
      }
    }));

    const handleEdit = () => {
      setEditing(true);
    }

    return (
      <>
      <Grid container spacing={1} columns={12}>
        <Grid item xs>
          <img
            className="w-20 h-20 mr-12 my-16"
            src={image}
            alt={name}
          />
        </Grid>
        {/* non editing ui*/}
        <Grid item xs={isEditable? 10: 11} display={editing? 'none': 'flex'}>
          <Grid
            container
            direction="column"
            justifyContent="center"
          >
            <Typography variant='body' color='#737373' fontSize="12px">{label}</Typography>
            <Typography variant='body'>{value}</Typography>
          </Grid>
        </Grid>
        {/* editing ui*/}
        <Grid item xs={11} display={editing? 'flex': 'none'}>
          <TextField
            variant="outlined"
            fullWidth
            label={label}
            name={name}
            onBlur={onBlur}
            onChange={onChange}
            value={value}
            helperText={helperText}
            error={error}
            InputProps={{endAdornment: (<SaveButton onSubmit={onSubmit} isSubmitting={isSubmitting}/>)}}
          />
        </Grid>
        <Grid item xs={1} display={!editing && isEditable? 'flex': 'none'}>
          <Tooltip title="Edit">
            <IconButton
              key={`action-${name}`}
              size="small"
              onClick={handleEdit}
            >
              <EditIcon/>
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
      </>
    );
  }
);

export default CustomTextField;
