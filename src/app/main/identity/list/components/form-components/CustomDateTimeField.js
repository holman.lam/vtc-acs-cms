
import { forwardRef, useEffect, useState, useImperativeHandle, useCallback } from 'react';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import Grid from '@mui/material/Grid';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SaveButton from './SaveButton'
import { extractDateFromDateTime, extractTimeFromDateTime } from 'app/utils/date'
import _ from 'lodash';

const CustomDateTimeField = forwardRef(
  (
    {
      isEditable,
      image,
      
      dateLabel,
      dateName,
      dateValue,
      dateHelperText,
      dateError,
      dateOnChange,

      timeLabel,
      timeName,
      timeValue,
      timeHelperText,
      timeError,
      timeOnChange,

      isSubmitting,
      onSubmit,
      onBlur,
      onChange,
    },
    ref
  ) => {
    const [editing, setEditing] = useState(false);
    
    useEffect(() => {
      
    }, []);
    
    useImperativeHandle(ref, () => ({
      closeEdit() {
        setEditing(false);
      }
    }));

    const handleEdit = () => {
      setEditing(true);
    }

    return (
      <>
      <Grid container spacing={1} columns={12}>
        <Grid item xs>
          <img
            className="w-20 h-20 mr-12 my-16"
            src={image}
            alt={name}
          />
        </Grid>
        {/* non editing ui*/}
        <Grid item xs={isEditable? 5: 5} display={editing? 'none': 'flex'}>
          <Grid
            container
            direction="column"
            justifyContent="center"
          >
            <Typography variant='body' color='#737373' fontSize="12px">{dateLabel}</Typography>
            <Typography variant='body'>{extractDateFromDateTime(dateValue)}</Typography>
          </Grid>
        </Grid>
        {/* editing ui*/}
        <Grid item xs={5} display={editing? 'flex': 'none'}>
          <DatePicker
            label={dateLabel}
            value={dateValue}
            onChange={dateOnChange}
            renderInput={(params) => (
              <TextField
                {...params}
                error={dateError}
                helperText={dateHelperText}
                // sx={{ flex: 2 / 3 }}
              />
            )}
          />
        </Grid>

        {/* non editing ui*/}
        <Grid item xs={isEditable? 5: 6} display={editing? 'none': 'flex'}>
          <Grid
            container
            direction="column"
            justifyContent="center"
          >
            <Typography variant='body' color='#737373' fontSize="12px">{timeLabel}</Typography>
            <Typography variant='body'>{extractTimeFromDateTime(timeValue)}</Typography>
          </Grid>
        </Grid>
        {/* editing ui*/}
        <Grid item xs={6} display={editing? 'flex': 'none'}>
          <TimePicker
            ampm={false}
            label={timeLabel}
            value={timeValue}
            onChange={timeOnChange}
            renderInput={(params) => (
              <TextField
                {...params}
                error={timeError}
                helperText={timeHelperText}
                // sx={{ flex: 1 / 3 }}
              />
            )}
          />
        </Grid>
        <Grid item xs={1} display={!editing && isEditable? 'flex': 'none'}>
          <Tooltip title="Edit">
            <IconButton
              key={`action-${dateName}`}
              size="small"
              onClick={handleEdit}
            >
              <EditIcon/>
            </IconButton>
          </Tooltip>
        </Grid>

        {/* new line for submiting*/}
        <Grid item xs={1} display={editing? 'flex': 'none'}>
        </Grid>
        <Grid item xs={11} display={editing? 'flex': 'none'}>
          <SaveButton onSubmit={onSubmit} isSubmitting={isSubmitting}/>
        </Grid>
      </Grid>
      </>
    );
  }
);

export default CustomDateTimeField;
