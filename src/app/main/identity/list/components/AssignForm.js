import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Toolbar from '@mui/material/Toolbar';
import TextField from '@mui/material/TextField';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { assignIdentities } from '@api';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import { IconButton } from '@mui/material';
import Divider from '@mui/material/Divider';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { Emitter, Event } from '@emitter';
import { closeDialog } from 'app/store/fuse/dialogSlice';
import _ from 'lodash';
import { useDepartments, useReaders } from 'app/hooks';
import { extractTimeFromDateTime } from 'app/utils/date';

const datLabels = {
  monday: 'Monday',
  tuesday: 'Tuesday',
  wednesday: 'Wednesday',
  thursday: 'Thursday',
  friday: 'Friday',
  saturday: 'Saturday',
  sunday: 'Sunday',
  // publicHoliday: 'Public Holiday',
};

const dayInitValues = {
  enabled: false,
  times: [
    {
      from: null,
      to: null,
    },
  ],
};

function AssignForm({ selectedIds }) {
  const dispatch = useDispatch();
  const [{ data: departments }] = useDepartments();
  const [{ res: readers }] = useReaders();

  const formik = useFormik({
    initialValues: {
      id: undefined,
      readerIds: [],
      startDate: null,
      endDate: null,
      monday: dayInitValues,
      tuesday: dayInitValues,
      wednesday: dayInitValues,
      thursday: dayInitValues,
      friday: dayInitValues,
      saturday: dayInitValues,
      sunday: dayInitValues,
      // publicHoliday: dayInitValues,
    },
    validateOnMount: true,
    validationSchema: Yup.object({ 
      startDate: Yup.date().nullable().required().label('Date of start'),
      endDate: Yup.date().nullable().required().label('Date of end'),
      monday: Yup.object(),
      tuesday: Yup.object(),
      wednesday: Yup.object(),
      thursday: Yup.object(),
      friday: Yup.object(),
      saturday: Yup.object(),
      sunday: Yup.object(),
      publicHoliday: Yup.object(),
    }),
    onSubmit: async (values, helpers) => {
      const data = {
        details: convertDayInitValuesIntoDetails(values),
        effectiveDate: values.startDate,
        expiryDate: values.endDate,
        identityId: selectedIds,
        readerId: values.readerIds,
      }
      await handleUpdateRecord(data, helpers)
    },
  });

  async function handleUpdateRecord(data, helpers) {
    const res = await assignIdentities({ data });
    if (res.ok) {
      Emitter.emit(Event.CMS_USER_UPDATED, res.data);
      dispatch(closeDialog());
    }
  }

  const selectAllReader = (e) => {
    console.log("selectAllReader");
    const { checked } = e.target;
    
    if (checked) {
      let readerIds = [];
      readers.map((reader)=>{
        readerIds = [...readerIds, reader.id];
      });
      formik.setFieldValue("readerIds", readerIds);
    } else {
      formik.setFieldValue("readerIds", []);
    }
  }
  const convertDayInitValuesIntoDetails = (values) => {
    const weekdays = {
      monday: "MON",
      tuesday: "TUE",
      wednesday: "WED",
      thursday: "THU",
      friday: "FRI",
      saturday: "SAT",
      sunday: "SUN",
      // publicHoliday: "PUB",
    };
    let details = [];

    Object.keys(weekdays).map(function(k, i) {
      if(values[k].enabled){
        values[k].times.map((time) =>{
          const detail = {
            day: weekdays[k],
            fromTime: extractTimeFromDateTime(time.from),
            toTime: extractTimeFromDateTime(time.to)
          }
          details = [...details, detail];
        }); 
      }
    });
    return details;
  } 

  return (
    <>
      <AppBar position="static" color="secondary" elevation={0}>
        <Toolbar className="flex w-full">
          <Typography variant="subtitle1" color="inherit">
            Assign
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        <div className="flex flex-row items-center">
          <img
            className="w-20 h-20 mr-12 my-16"
            src="assets/images/icons/reader.svg"
            alt="Reader"
          />

          <Typography>Assign Reader(s)</Typography>
        </div>

        <Grid container className="pl-32">
          <Grid item xs={12}>
            <FormControlLabel control={<Checkbox onChange={selectAllReader}/>} label="All Readers" />
          </Grid>

          {readers.map((reader)=>(
            <Grid item xs={6}>
              <FormControlLabel control={
                <Checkbox 
                  key={reader.id}
                  checked={formik.values.readerIds.includes(reader.id)}
                  value={reader.id}
                  onChange={e => {
                    const { checked } = e.target;
                    if (checked) {
                      formik.setFieldValue("readerIds", [...formik.values.readerIds, reader.id]);
                    } else {
                      formik.setFieldValue(
                        "readerIds",
                        formik.values.readerIds.filter((v) => v !== reader.id)
                      );
                    }
                  }}/>
              } 
                label={reader.name} 
              />
            </Grid>
          ))}
        </Grid>

        <Divider flexItem className="my-12" />

        <div className="flex flex-row items-center">
          <img
            className="w-20 h-20 mr-12 my-16"
            src="assets/images/icons/calendar.svg"
            alt="Calendar"
          />

          <Typography>Date Period</Typography>
        </div>

        <div className="flex flex-col mb-24 pl-32 items-center">
          <DatePicker
            label="Date of start"
            value={formik.values.startDate}
            onChange={(newValue) => {
              formik.setFieldValue('startDate', newValue);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                fullWidth
                error={Boolean(formik.touched.startDate && formik.errors.startDate)}
                helperText={
                  Boolean(formik.touched.startDate && formik.errors.startDate) &&
                  formik.errors.startDate
                }
              />
            )}
          />
          <Typography>to</Typography>
          <DatePicker
            label="Date of end"
            value={formik.values.endDate}
            onChange={(newValue) => {
              formik.setFieldValue('endDate', newValue);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                fullWidth
                error={Boolean(formik.touched.endDate && formik.errors.endDate)}
                helperText={
                  Boolean(formik.touched.endDate && formik.errors.endDate) && formik.errors.endDate
                }
              />
            )}
          />
        </div>

        <div className="flex flex-row items-center">
          <img
            className="w-20 h-20 mr-12 my-16"
            src="assets/images/icons/time.svg"
            alt="Calendar"
          />

          <Typography>Day(s) & Time Range</Typography>
        </div>

        <div className="flex flex-col mb-24 pl-32">
          {[
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
            // 'publicHoliday',
          ].map((day) => {
            const dayValues = formik.values[day];

            return (
              <Grid sx={{ mb: 2 }}>
                <Grid>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={dayValues.enabled}
                        onChange={formik.handleChange}
                        name={`${day}.enabled`}
                      />
                    }
                    label={datLabels[day]}
                  />
                </Grid>

                {dayValues.times.map((time, index) => {
                  return (
                    <Grid container className="items-center mb-2">
                      <TimePicker
                        ampm={false}
                        value={time.from}
                        onChange={(newValue) => {
                          formik.setFieldValue(`${day}.times[${index}].from`, newValue);
                        }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            error={Boolean(
                              _.get(formik.touched, `${day}.times[${index}].from`) &&
                                _.get(formik.errors, `${day}.times[${index}].from`)
                            )}
                            helperText={
                              Boolean(
                                _.get(formik.touched, `${day}.times[${index}].from`) &&
                                  _.get(formik.errors, `${day}.times[${index}].from`)
                              ) && _.get(formik.errors, `${day}.times[${index}].from`)
                            }
                            size="small"
                            sx={{ flex: 1 }}
                          />
                        )}
                      />

                      <Typography sx={{ mx: 2 }}>to</Typography>

                      <TimePicker
                        ampm={false}
                        value={time.to}
                        onChange={(newValue) => {
                          formik.setFieldValue(`${day}.times[${index}].to`, newValue);
                        }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            error={Boolean(
                              _.get(formik.touched, `${day}.times[${index}].to`) &&
                                _.get(formik.errors, `${day}.times[${index}].to`)
                            )}
                            helperText={
                              Boolean(
                                _.get(formik.touched, `${day}.times[${index}].to`) &&
                                  _.get(formik.errors, `${day}.times[${index}].to`)
                              ) && _.get(formik.errors, `${day}.times[${index}].to`)
                            }
                            size="small"
                            sx={{ flex: 1 }}
                          />
                        )}
                      />

                      <IconButton size="small" onClick={() => {formik.setFieldValue(`${day}.times`,  [...dayValues.times, {from: null, to: null,}])}}>
                        <AddCircleIcon fontSize="10" />
                      </IconButton>
                    </Grid>
                  );
                })}
              </Grid>
            );
          })} 
        </div>
      </DialogContent>
      <DialogActions className="px-24 pb-24">
        <Button disabled={formik.isSubmitting} onClick={() => formik.handleSubmit()}>
          Add
        </Button>
      </DialogActions>
    </>
  );
}

export default AssignForm;
