import { useEffect, useRef } from 'react';
import { useNavigate } from "react-router-dom";
import Table from 'app/shared-components/Table';
import { useDebounce } from 'app/hooks';
import { useApprovalTable } from './ApprovalTable.context';
import { getAccessBatch } from '@api';
import EditIcon from '@mui/icons-material/Edit';
import { convertApiDateTimeToDisplayDateTime } from 'app/utils/date';

const columns = [
  { id: 'id', label: 'Approval No.', minWidth: 170 },
  { id: 'createTime', label: 'Assigned Time', minWidth: 170, render: ({ rowData }) => convertApiDateTimeToDisplayDateTime(rowData.createTime) },
  { id: 'createdBy', label: 'Assigned By', minWidth: 150},
  { id: 'status', label: 'Status', minWidth: 150 },
];

function ApprovalTable(props) {
  const tableRef = useRef(null);
  const navigate = useNavigate();
  const { status, searchValue } = useApprovalTable();
  const debouncedsearchValue = useDebounce(searchValue, 500);
  

  function reloadTable() {
    tableRef.current.reload();
  }

  useEffect(() => {
    reloadTable();
  }, [status, debouncedsearchValue]);
  
  const handleAccessBatchDetails = (id) => {
    navigate(`/identity/approval/${id}`);
  };

  const dataSource = ({ page, pageSize, sort }) => {
    const data = {
      currentPage: page,
      pageSize,
      filterQuery: "",
      order: [
        {
          column: "id",
          dir: sort.direction,
        },
      ],
    };
    return new Promise((resolve, reject) => {
      getAccessBatch({
        data,
      }).then((response) => {
        if (response.ok) {
          return resolve({
            data: response.data.content,
            count: response.data.total,
          });
        }
        return reject(response?.data?.errorMessage || response.problem);
      });
    });
  };

  return (
    <Table
      ref={tableRef}
      columns={columns}
      actions={[
        (rowData) => ({
          icon: <EditIcon />,
          tooltip: 'Edit',
          onClick: () => handleAccessBatchDetails(rowData.id),
        }),
      ]}
      dataSource={dataSource}
      defaultSort={{ direction: 'asc', id: 'id' }}
    />
  );
}

export default ApprovalTable;
