import { styled } from '@mui/material/styles';
import MuiCard from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { useDispatch } from 'react-redux';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import { useApprovalTable } from './ApprovalTable.context';

const Card = styled(MuiCard)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    boxShadow: 'none',
    background: 'none',
  },
}));

function LeftSidebar(props) {
  const { status, setStatus } = useApprovalTable();

  return (
    <div>
      <Card className="m-12">
        <CardContent className="flex flex-col items-center">
          <ListItem disablePadding>
            <ListItemButton selected={status === 'waiting'} onClick={() => setStatus('waiting')}>
              <img
                className="w-20 h-20 mr-12"
                src="assets/images/icons/all_contacts.svg"
                alt="Waiting"
              />
              <ListItemText primary="Waiting" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton selected={status === 'approved'} onClick={() => setStatus('approved')}>
              <img
                className="w-20 h-20 mr-12"
                src="assets/images/icons/students.svg"
                alt="Approved"
              />
              <ListItemText primary="Approved" />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton selected={status === 'rejected'} onClick={() => setStatus('rejected')}>
              <img className="w-20 h-20 mr-12" src="assets/images/icons/local.svg" alt="Rejected" />
              <ListItemText primary="Rejected" />
            </ListItemButton>
          </ListItem>
        </CardContent>
      </Card>
    </div>
  );
}

export default LeftSidebar;
