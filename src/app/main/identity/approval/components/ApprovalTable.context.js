import { createContext, useState, useContext } from 'react';

export const ApprovalTableContext = createContext({
  status: 'waiting',
  setStatus: () => {},
  searchValue: '',
  setSearchValue: () => {},
});

export const ApprovalTableProvider = (props) => {
  const { children } = props;
  const [status, setStatus] = useState('waiting');
  const [searchValue, setSearchValue] = useState('');

  return (
    <ApprovalTableContext.Provider
      value={{
        status,
        setStatus,
        searchValue,
        setSearchValue,
      }}
    >
      {children}
    </ApprovalTableContext.Provider>
  );
};

export const ApprovalTableConsumer = ApprovalTableContext.Consumer;
export const useApprovalTable = () => useContext(ApprovalTableContext);
