import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import LeftSidebar from './components/LeftSidebar';
import Table from './components/ApprovalTable';
import { ApprovalTableProvider } from './components/ApprovalTable.context';

function Page() {
  return (
    <ApprovalTableProvider>
      <FusePageCarded
        header={<Header />}
        leftSidebarOpen
        leftSidebarContent={<LeftSidebar />}
        content={<Table />}
        scroll="content"
      />
    </ApprovalTableProvider>
  );
}

export default Page;
