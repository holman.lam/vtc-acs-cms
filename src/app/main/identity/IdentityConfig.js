import IdentitiesList from './list';
import IdentitiesApproval from './approval';
import IdentitiesApprovalDetails from './approval-details';

const IdentityConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: '/identity/list',
      element: <IdentitiesList />,
    },
    {
      path: '/identity/approval',
      element: <IdentitiesApproval />,
    },
    {
      path: '/identity/approval/:id',
      element: <IdentitiesApprovalDetails />,
    },
  ],
};

export default IdentityConfig;
