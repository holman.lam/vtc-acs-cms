import { useEffect, useRef, useState } from 'react';
import { useNavigate } from "react-router-dom";
import { styled } from '@mui/material/styles';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import MuiCard from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import { openDialog } from 'app/store/fuse/dialogSlice';
import { getAccessBatchById, approveAccessBatch, rejectAccessBatch } from '@api';
import { useApprovalDetailsTable } from './ApprovalDetailsTable.context';
import { extractDateFromDateTime } from 'app/utils/date'
import { closeDialog } from 'app/store/fuse/dialogSlice';


const Card = styled(MuiCard)(({ theme }) => ({
  [theme.breakpoints.down('lg')]: {
    boxShadow: 'none',
    background: 'none',
  },
}));

function LeftSidebar(props) {
  const { status, setStatus } = useApprovalDetailsTable();
  const { id } = useParams();
  const [accessBatch, setAccessBatch] = useState({});
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    getAccessBatchById({ id }).then((response) => {
      setAccessBatch(response.data);
    });
  }, []);

  const openConfirmationDialog = (isApprove) => {
    dispatch(openDialog({
      children: (
          <>
            <AppBar position="static" color="secondary" elevation={0}>
              <Toolbar className="flex w-full">
                <Typography variant="subtitle1" color="inherit">
                  {isApprove ? 'Approve' : 'Reject'} All
                </Typography>
              </Toolbar>
            </AppBar>
            <DialogContent>
              <Typography variant="body">
                Confirm to {isApprove ? 'approve' : 'reject'} All?
              </Typography>
            </DialogContent>
            <DialogActions className="px-24 pb-24">
              <Button onClick={() => handleSubmit(isApprove)} color="primary">
                Confirm
              </Button>
              <Button onClick={() => handleSubmit(isApprove)} style={{backgroundColor:"#666666", color: "#FFFFFF"}} >
                Cancel
              </Button>
            </DialogActions>
          </>
           )
       }))
  };
  const handleSubmit = (isApprove) => {
    if(isApprove){
      handleApprove();
    } else {
      handleReject();
    }
  }
  const handleApprove = () => {
    const data = {
      batchId: id
    };
    approveAccessBatch({ data }).then((response) => {
      navigate('/identity/approval');
      dispatch(closeDialog());
    });
  }

  const handleReject = () => {
    const data = {
      batchId: id
    };
    rejectAccessBatch({ data }).then((response) => {
      navigate('/identity/approval');
      dispatch(closeDialog());
    });
  }
  return (
    <div>
      <Card className="m-12">
        <CardContent className="flex flex-col items-center">
          <Grid container spacing={1} columns={12}>
            <Grid item xs={12}>Assigned Details</Grid>
            <Grid item xs={2} className="mt-12">
              <img
                className="w-20 h-20"
                src="assets/images/icons/students.svg"
                alt="Approved"
              />
            </Grid>
            <Grid item xs={10} className="mt-12">
              <Typography variant='body' color='#666666'>Devices</Typography>
            </Grid>
            <Grid item xs={12}>
              {/* TODO: Gate */}
              {accessBatch.readers?accessBatch.readers.map((reader, i) => {
                if(i == 0){
                  return (
                    <>{reader.name}</>
                  )
                } else {
                  return (
                    <>, {reader.name}</>
                  )
                }
              }):""}
            </Grid>

            <Grid item xs={2} className="mt-12">
              <img
                className="w-20 h-20"
                src="assets/images/icons/students.svg"
                alt="Approved"
              />
            </Grid>
            <Grid item xs={10} className="mt-12">
              <Typography variant='body' color='#666666'>Date Period</Typography>
            </Grid>
            <Grid item xs={12}>
              {accessBatch.effectiveDate? (<>{extractDateFromDateTime(accessBatch.effectiveDate)} to {extractDateFromDateTime(accessBatch.expiryDate)}</>) : ""}
            </Grid>

            <Grid item xs={2} className="mt-12">
              <img
                className="w-20 h-20"
                src="assets/images/icons/students.svg"
                alt="Approved"
              />
            </Grid>
            <Grid item xs={10} className="mt-12">
              <Typography variant='body' color='#666666'>Day(s) & Time Range</Typography>
            </Grid>
            <Grid item xs={12}>
              Monday 09:00 - 23:00
            </Grid>
            <Grid item xs={12}>
              Friday 08:00 - 11:00
            </Grid>

            <Grid item xs={12} className="mt-24">
              <Button 
                fullWidth
                variant="contained" 
                style={{backgroundColor:"#2dce82", color: "#FFFFFF"}} 
                onClick={() => openConfirmationDialog(true)}>
                  Approve All
              </Button>
            </Grid>
            <Grid item xs={12}>
              <Button 
                fullWidth
                variant="contained" 
                style={{backgroundColor:"#d90366", color: "#FFFFFF"}} 
                onClick={() => openConfirmationDialog(false)}>
                  Reject All
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </div>
  );
}

export default LeftSidebar;
