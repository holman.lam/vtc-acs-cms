import Typography from '@mui/material/Typography';
import { motion } from 'framer-motion';
import Hidden from '@mui/material/Hidden';
import { useApprovalDetailsTable } from './ApprovalDetailsTable.context';
import SearchBox from './SearchBox';

function Header(props) {
  const { searchValue, setSearchValue } = useApprovalDetailsTable();

  return (
    <div className="flex flex-row flex-1 w-full space-y-8 sm:space-y-0 items-center justify-between py-12 lg:py-32 lg:px-32">
      <div className="flex items-center w-256">
        <img
          className="w-20 h-20 lg:w-32 lg:h-32 mr-12 lg:mr-24"
          src="assets/images/navigation-icon/account.svg"
          alt="Account"
        />

        <Typography
          component={motion.span}
          initial={{ x: -20 }}
          animate={{ x: 0, transition: { delay: 0.2 } }}
          delay={300}
          color="primary.contrastText"
          className="flex text-16 lg:text-20 font-medium"
        >
          Identity/Approval
        </Typography>
      </div>

      <Hidden lgDown>
        <SearchBox />
      </Hidden>

      <div className="w-256" />
    </div>
  );
}

export default Header;
