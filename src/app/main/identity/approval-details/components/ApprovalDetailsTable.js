import { useEffect, useRef } from 'react';
import { useParams } from 'react-router-dom';
import Table from 'app/shared-components/Table';
import { useDebounce } from 'app/hooks';
import { useApprovalDetailsTable } from './ApprovalDetailsTable.context';
import { getAccessBatchIdentityById } from '@api';
import EditIcon from '@mui/icons-material/Edit';
import { convertApiDateTimeToDisplayDateTime } from 'app/utils/date';

const columns = [
  { id: 'cna', label: 'CNA', minWidth: 170 },
  { id: 'smartCardId', label: 'Smart Card ID.', minWidth: 170 },
  { id: 'firstName', label: 'First Name', minWidth: 150 },
  { id: 'lastName', label: 'Last Name', minWidth: 150 },
  { id: 'chineseName', label: 'Chinese Name', minWidth: 170 },
  { id: 'department', label: 'Department', minWidth: 100 },
  { id: 'accountType', label: 'Account Type', minWidth: 100 },
  { id: 'courseNo', label: 'Course No.', minWidth: 100 },
  { id: 'module', label: 'Module', minWidth: 100 },
  { id: 'year', label: 'Year', minWidth: 100 },
  { id: 'class', label: 'Class', minWidth: 100 },
  { id: 'site', label: 'Site', minWidth: 100 },
  { id: 'dateOfEntry', label: 'Date Of Entry', minWidth: 170, render: ({ rowData }) => convertApiDateTimeToDisplayDateTime(rowData.dateOfEntry)},
  { id: 'expiryDate', label: 'Expiry Date', minWidth: 170, render: ({ rowData }) => convertApiDateTimeToDisplayDateTime(rowData.expiryDate) },
  { id: '', label: 'Building', minWidth: 100 },
];

function ApprovalDetailsTable(props) {
  const tableRef = useRef(null);
  const { status, searchValue } = useApprovalDetailsTable();
  const debouncedsearchValue = useDebounce(searchValue, 500);
  const { id } = useParams();

  function reloadTable() {
    tableRef.current.reload();
  }

  useEffect(() => {
    reloadTable();
  }, [status, debouncedsearchValue]);

  const dataSource = ({ page, pageSize, sort }) => {
    const data = {
      pageNo: page,
      size: pageSize,
    };
    return new Promise((resolve, reject) => {
      getAccessBatchIdentityById({
        id, 
        data,
      }).then((response) => {
        if (response.ok) {
          return resolve({
            data: response.data.content,
            count: response.data.total,
          });
        }
        return reject(response?.data?.errorMessage || response.problem);
      });
    });
  };

  return (
    <Table
      ref={tableRef}
      columns={columns}
      dataSource={dataSource}
      defaultSort={{ direction: 'asc', id: 'id' }}
    />
  );
}

export default ApprovalDetailsTable;
