import { createContext, useState, useContext } from 'react';

export const ApprovalDetailsTableContext = createContext({
  status: 'waiting',
  setStatus: () => {},
  searchValue: '',
  setSearchValue: () => {},
});

export const ApprovalDetailsTableProvider = (props) => {
  const { children } = props;
  const [status, setStatus] = useState('waiting');
  const [searchValue, setSearchValue] = useState('');

  return (
    <ApprovalDetailsTableContext.Provider
      value={{
        status,
        setStatus,
        searchValue,
        setSearchValue,
      }}
    >
      {children}
    </ApprovalDetailsTableContext.Provider>
  );
};

export const ApprovalDetailsTableConsumer = ApprovalDetailsTableContext.Consumer;
export const useApprovalDetailsTable = () => useContext(ApprovalDetailsTableContext);
