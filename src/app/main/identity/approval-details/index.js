import FusePageCarded from '@fuse/core/FusePageCarded';
import Header from './components/Header';
import LeftSidebar from './components/LeftSidebar';
import Table from './components/ApprovalDetailsTable';
import { ApprovalDetailsTableProvider } from './components/ApprovalDetailsTable.context';

function Page() {
  return (
    <ApprovalDetailsTableProvider>
      <FusePageCarded
        header={<Header />}
        leftSidebarOpen
        leftSidebarContent={<LeftSidebar />}
        content={<Table />}
        scroll="content"
      />
    </ApprovalDetailsTableProvider>
  );
}

export default Page;
