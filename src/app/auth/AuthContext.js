import * as React from 'react';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import FuseSplashScreen from '@fuse/core/FuseSplashScreen';
import { login as loginApi, getMe } from '@api';
import { logoutUser, setUser } from 'app/store/userSlice';
import { getAccessToken, setAccessToken } from './TokenStorage';

const AuthContext = React.createContext();

function AuthProvider({ children }) {
  const [isAuthenticated, setIsAuthenticated] = useState(undefined);
  const [waitAuthCheck, setWaitAuthCheck] = useState(true);
  const dispatch = useDispatch();

  // This function is checking the user is logged in
  useEffect(() => {
    function success(user) {
      Promise.all([dispatch(setUser(user))]).then((values) => {
        setWaitAuthCheck(false);
        setIsAuthenticated(true);
      });
    }

    function pass() {
      setWaitAuthCheck(false);
      setIsAuthenticated(false);
    }

    const handleAuthentication = async () => {
      const accessToken = getAccessToken();
      if (!accessToken) {
        pass();
        return;
      }

      const res = await getMe();

      if (res.ok) {
        const user = res.data;
        // add dummy role
        user.role = ['admin'];

        success(user);
      } else {
        pass();
      }
    };

    handleAuthentication();
  }, [dispatch]);

  const login = (data) => {
    return new Promise((resolve, reject) => {
      loginApi({ data }).then((loginRes) => {
        if (loginRes.ok) {
          const accessToken = loginRes.data.access_token;
          setAccessToken(accessToken);

          getMe()
            .then((getMeRes) => {
              if (getMeRes.ok) {
                const user = getMeRes.data;
                // add dummy role
                user.role = ['admin'];

                Promise.all([dispatch(setUser(user))]).then((values) => {
                  setWaitAuthCheck(false);
                  setIsAuthenticated(true);
                });
                resolve(user);
              } else {
                reject(new Error('Invalid username or password.'));
              }
            })
            .catch((error) => {
              reject(new Error('Invalid username or password.'));
            });
        } else {
          reject(new Error('Invalid username or password.'));
        }
      });
    });
  };

  const logout = () => {
    setAccessToken(null);
    setWaitAuthCheck(false);
    setIsAuthenticated(false);
    dispatch(logoutUser());
  };

  return waitAuthCheck ? (
    <FuseSplashScreen />
  ) : (
    <AuthContext.Provider value={{ isAuthenticated, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
}

function useAuth() {
  const context = React.useContext(AuthContext);
  if (context === undefined) {
    throw new Error('useAuth must be used within a AuthProvider');
  }
  return context;
}

export { AuthProvider, useAuth };
