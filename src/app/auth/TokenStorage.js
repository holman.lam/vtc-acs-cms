const getAccessToken = () => {
  return window.localStorage.getItem('jwt_access_token');
};

const setAccessToken = (accessToken) => {
  if (accessToken) {
    localStorage.setItem('jwt_access_token', accessToken);
  } else {
    localStorage.removeItem('jwt_access_token');
  }
};

export { getAccessToken, setAccessToken };
