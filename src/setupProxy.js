const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    createProxyMiddleware('/api', {
      target: 'https://qbs-vtc.sukisunny.com',
      changeOrigin: true,
      pathRewrite: { '^/api': '' },
    })
  );
};
