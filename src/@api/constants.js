export const LOGIN = '/auth/oauth/token';
export const GET_ME = '/core/user/me';

export const GET_IDENTITIES = '/core/identity/all/page';
export const GET_IDENTITY = (id) => `/core/identity/id/${id}`;
export const CREATE_IDENTITY = '/core/identity/create';
export const UPDATE_IDENTITY = '/core/identity/update';
export const ASSIGN_IDENTITIES = '/core/identity/assign';

export const GET_ACCESS_BATCH = '/core/identity/access-batch/page';
export const GET_ACCESS_BATCH_BY_ID = (id) => `/core/identity/access-batch/${id}`;
export const GET_ACCESS_BATCH_IDENTITY_BY_ID = (id) => `/core/identity/access-batch/${id}/identity`;
export const APPROVE_ACCESS_BATCH = '/core/identity/access/approve';
export const REJECT_ACCESS_BATCH = '/core/identity/access/reject';

export const GET_ACCESS_LOGS = '/core/identity/access-log/page';

export const GET_LOCATIONS = '/core/location';

export const GET_READERS = '/core/reader/all/page';
export const GET_READER = (id) => `/core/reader/id/${id}`;
export const CREATE_READER = '/core/reader/create';
export const UPDATE_READER = '/core/reader/update';

export const GET_SCHEDULER_GATE = '/core/scheduler/gate';
export const GET_SCHEDULER_DB = '/core/scheduler/db';

export const CREATE_SCHEDULER_GATE = '/core/scheduler/gate';

export const GET_CUSTOM_FIELDS = '/core/custom-field/all';
export const CREATE_CUSTOM_FIELD = '/core/custom-field/create';
export const UPDATE_CUSTOM_FIELD = '/core/custom-field/update';
export const ENABLE_CUSTOM_FIELD = '/core/custom-field/enable';
export const DISABLE_CUSTOM_FIELD = '/core/custom-field';

// This api can be used for getting single app account or cms user
export const GET_ACCOUNT = (username) => `/core/user/id/${username}`;

export const GET_APP_ACCOUNTS = '/core/user/all/page/app-account';
export const CREATE_APP_ACCOUNT = '/core/user/create/app-account';
export const UPDATE_APP_ACCOUNT = (username) => `/core/user/update/app-account/${username}`;

export const GET_USER_ACCOUNTS = '/core/user/all/page/cms-account';
export const CREATE_USER_ACCOUNT = '/core/user/create/cms-account';
export const UPDATE_USER_ACCOUNT = (username) => `/core/user/update/cms-account/${username}`;

export const GET_DEPARTMENTS = '/core/department';
export const ASSIGN_DEPARTMENT = '/core/department/assign';
