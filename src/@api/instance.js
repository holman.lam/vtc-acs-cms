import axios from 'axios';
import { create } from 'apisauce';

const instance = axios.create({
  baseURL: '/api',
  timeout: 30000, // 30 sec
});

// Add a request interceptor
instance.interceptors.request.use(
  (config) => {
    const token = window.localStorage.getItem('jwt_access_token');
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

const apisauceInstance = create({
  baseURL: '/api',
  axiosInstance: instance,
});

export default apisauceInstance;
