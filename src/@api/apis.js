import * as ApiPaths from './constants';
import instance from './instance';

export async function login(action) {
  const params = new URLSearchParams();
  params.append('client_id', 'cms-user');
  params.append('client_secret', 'cms-user-secret');
  params.append('grant_type', 'password');
  params.append('username', action.data.username);
  params.append('password', action.data.password);

  const response = await instance.post(ApiPaths.LOGIN, params, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: '',
    },
  });
  return response;
}

export async function getMe() {
  return instance.get(ApiPaths.GET_ME);
}

// Identity
export async function getIdentities(action = {}) {
  return instance.post(ApiPaths.GET_IDENTITIES, action.data);
}

export async function getIdentity(action) {
  return instance.get(ApiPaths.GET_IDENTITY(action.id));
}

export async function createIdentity(action) {
  return instance.post(ApiPaths.CREATE_IDENTITY, action.data);
}

export async function updateIdentity(action) {
  return instance.put(ApiPaths.UPDATE_IDENTITY, action.data);
}

export async function assignIdentities(action) {
  return instance.post(ApiPaths.ASSIGN_IDENTITIES, action.data);
}

// Access Batch
export async function getAccessBatch(action = {}) {
  return instance.post(ApiPaths.GET_ACCESS_BATCH, action.data);
}
export async function getAccessBatchById(action) {
  return instance.get(ApiPaths.GET_ACCESS_BATCH_BY_ID(action.id));
}
export async function getAccessBatchIdentityById(action) {
  return instance.get(ApiPaths.GET_ACCESS_BATCH_IDENTITY_BY_ID(action.id), action.data);
}
export async function approveAccessBatch(action = {}) {
  return instance.post(ApiPaths.APPROVE_ACCESS_BATCH, action.data);
}
export async function rejectAccessBatch(action = {}) {
  return instance.post(ApiPaths.REJECT_ACCESS_BATCH, action.data);
}

//AccessLog
export async function getAccessLogs(action = {}) {
  return instance.post(ApiPaths.GET_ACCESS_LOGS, action.data);
}

// Gate Scheduler
export async function getSchedulerGate(action = {}) {
  return instance.get(ApiPaths.GET_SCHEDULER_GATE, action.data);
}

export async function getSchedulerDB(action = {}) {
  return instance.get(ApiPaths.GET_SCHEDULER_DB, action.data);
}

export async function createSchedulerGate(action = {}) {
  return instance.post(ApiPaths.CREATE_SCHEDULER_GATE, action.data);
}



// Custom Field
export async function getCustomFields(action = {}) {
  return instance.get(ApiPaths.GET_CUSTOM_FIELDS);
}

export async function createCustomField(action) {
  return instance.post(ApiPaths.CREATE_CUSTOM_FIELD, action.data);
}

export async function updateCustomField(action) {
  return instance.post(ApiPaths.UPDATE_CUSTOM_FIELD, action.data);
}

export async function enabledCustomField(action) {
  return instance.put(ApiPaths.ENABLE_CUSTOM_FIELD, action.data);
}

export async function deleteCustomField(action) {
  return instance.put(ApiPaths.DISABLE_CUSTOM_FIELD, action.data);
}

// Location
export async function getLocations(action = {}) {
  return instance.get(ApiPaths.GET_LOCATIONS);
}

// Reader
export async function getReaders(action = {}) {
  return instance.post(ApiPaths.GET_READERS, action.data);
}

export async function getReader(action) {
  return instance.get(ApiPaths.GET_READER(action.id));
}

export async function createReader(action) {
  return instance.post(ApiPaths.CREATE_READER, action.data);
}

export async function updateReader(action) {
  return instance.put(ApiPaths.UPDATE_READER, action.data);
}

// App Account
export async function getAppAccounts(action = {}) {
  return instance.post(ApiPaths.GET_APP_ACCOUNTS, action.data);
}

export async function getAppAccount(action) {
  return instance.get(ApiPaths.GET_ACCOUNT(action.username));
}

export async function createAppAccount(action) {
  return instance.post(ApiPaths.CREATE_APP_ACCOUNT, action.data);
}

export async function updateAppAccount(action) {
  return instance.put(ApiPaths.UPDATE_APP_ACCOUNT(action.username), action.data);
}

// User User
export async function getUserAccounts(action = {}) {
  return instance.post(ApiPaths.GET_USER_ACCOUNTS, action.data);
}

export async function getUserAccount(action) {
  return instance.get(ApiPaths.GET_ACCOUNT(action.username));
}

export async function createUserAccount(action) {
  return instance.post(ApiPaths.CREATE_USER_ACCOUNT, action.data);
}

export async function updateUserAccount(action) {
  return instance.put(ApiPaths.UPDATE_USER_ACCOUNT(action.username), action.data);
}

// Department
export async function getDepartments(action = {}) {
  return instance.get(ApiPaths.GET_DEPARTMENTS);
}

export async function assignDepartment(action) {
  return instance.post(ApiPaths.ASSIGN_DEPARTMENT, action.data);
}
