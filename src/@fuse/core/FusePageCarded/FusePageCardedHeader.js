import { ThemeProvider } from '@mui/material/styles';
import clsx from 'clsx';
import Hidden from '@mui/material/Hidden';
import { useSelector } from 'react-redux';
import { selectFuseNavbar } from 'app/store/fuse/navbarSlice';
import NavbarToggleButton from 'app/theme-layouts/shared-components/NavbarToggleButton';
import { selectFuseCurrentLayoutConfig, selectNavbarTheme } from 'app/store/fuse/settingsSlice';

function FusePageCardedHeader(props) {
  const config = useSelector(selectFuseCurrentLayoutConfig);
  const navbar = useSelector(selectFuseNavbar);
  const navbarTheme = useSelector(selectNavbarTheme);

  return (
    <div className={clsx('FusePageCarded-header')}>
      <div className={clsx('container flex')}>
        <Hidden lgUp>
          <ThemeProvider theme={navbarTheme}>
            <NavbarToggleButton className="w-40 h-40 p-0 m-12" />
          </ThemeProvider>
        </Hidden>
        {props.header && props.header}
      </div>
    </div>
  );
}

export default FusePageCardedHeader;
