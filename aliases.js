const aliases = (prefix = `src`) => ({
  '@fuse': `${prefix}/@fuse`,
  '@history': `${prefix}/@history`,
  '@lodash': `${prefix}/@lodash`,
  '@api': `${prefix}/@api`,
  '@emitter': `${prefix}/@emitter`,
  'app/store': `${prefix}/app/store`,
  'app/hooks': `${prefix}/app/hooks`,
  'app/shared-components': `${prefix}/app/shared-components`,
  'app/configs': `${prefix}/app/configs`,
  'app/theme-layouts': `${prefix}/app/theme-layouts`,
  'app/utils': `${prefix}/app/utils`,
  'app/AppContext': `${prefix}/app/AppContext`,
});

module.exports = aliases;
